# -*- encoding: ISO-8859-1 -*-
import Pyro4
import random
from subprocess import Popen
import os
import socket
import platform
from orion_2011_modele import *
from orion_2011_vue import *
from helper import Helper
from collections import OrderedDict

print(platform.platform())
print(platform.system())
print(platform.python_version_tuple())
print(os.getcwd())
print(os.environ)

class Toto(object):
    def __init__(self):
        self.n=1

t=Toto()
l=[t]
l2=[l[0]]   
del t
del l 
del l2[0]
print(l2)

class Controleur(object):
    def __init__(self):
        self.nom=""
        self.cadre=0
        self.mort = 0
        self.actions=[]
        self.serveurLocal=0
        self.serveur=0
        
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("gmail.com",80))
        #s.connect(("127.0.0.1",80))
        self.monip=s.getsockname()[0]
        s.close()
        
        print(self.monip)

        self.modele=Modele(self)
        #s=input("2")
        self.vue=Vue(self,self.modele.paramPartie["largeurFenetreJeu"],self.modele.paramPartie["hauteurFenetreJeu"],self.modele.paramPartie["x_espace"],self.modele.paramPartie["y_espace"],
                     self.modele.paramPartie["x_planete"],self.modele.paramPartie["y_planete"],self.modele.paramPartie["x_systeme"],self.modele.paramPartie["y_systeme"])
        #s=input("3")

    def creerServeur(self):
        pid = Popen(["python", "orion_2011_serveur.py"]).pid
        self.serveurLocal=1
        return pid
        
    def deces(self):
        self.mort = 1
        if self.serveur:
            self.serveur.deces(self.nom)
        
    def jeQuitte(self):
        if self.serveur:
            if self.mort == 0:
                self.serveur.deces(self.nom)
                self.serveur.jeQuitte(self.nom)
            else:
                self.serveur.jeQuitte(self.nom)
        
    def stopServeur(self):
        rep=self.serveur.quitter()
        print(rep)    
        self.serveur=0
        input("FERMER")
        
    def inscritClient(self,nom,leip):
        ad="PYRO:controleurServeur@"+leip+":54440"
        self.serveur=Pyro4.core.Proxy(ad)
        Pyro4.socketutil.setReuseAddr(self.serveur)
        
        rep=self.serveur.inscritClient(nom)
        if rep[0]:
            self.modele.rdseed=rep[2]
            random.seed(self.modele.rdseed)
            self.nom=nom
            #self.vue.canevasEspace.bind("<Button>",self.vue.changeCible)
            self.vue.afficheAttente()
            self.timerAttend()
        else:
            print("NON inscrit: recommencer avec un autre nom !")
        
    def demarrePartie(self):
        rep=self.serveur.demarrePartie()
        print(rep)
        
    def creerBot(self):
        pass
                
    def changeCible(self,monid,x,y):
        self.actions.append([self.nom,"changeCible",[monid,x,y]])

    def captureChat(self,txt):
        self.actions.append([self.nom,"captureChat",[txt]])

    def creerVaisseau(self,selection,type):
        self.actions.append([self.nom,"creerVaisseau",[type,selection.id]])
        print(type," pass� au modele")

    def creerBatiment(self,selection,type,etoile):
        print("A MARCHE PAS",selection,type,etoile)
        self.actions.append([self.nom,"creerBatiment",[type,selection.noPlanete,etoile.id]])
        print(type," pass� au modele")

    def creerStation(self,selection):
        self.actions.append([self.nom,"creerStation",[selection.id]])

    def creerPeon(self,selection):
        self.actions.append([self.nom,"creerPeon",[selection.id]])

    # ******  SECTION d'appels automatique
    def timerAttend(self):
        #print("attente1")
        if self.serveur:
            #print("attente2")
            rep=self.serveur.faitAction([self.nom,self.cadre,[]])
            if rep[0]:
                #print("attente3")
                self.modele.initPartie(rep[2][1][0][1])
                self.vue.initPartie(self.modele)
                self.vue.canevasEspace.bind("<Button>",self.vue.changeCible)
                #input("TATA")
                self.vue.centrerPlanete()
                self.vue.root.after(10,self.timerJeu)
                
            elif rep[0]==0:
                #print(rep[2])
                self.vue.afficheListeJoueurs(rep[2])
                self.vue.root.after(10,self.timerAttend)
        else:
            print("Aucun serveur attache")
        
    def timerJeu(self):
        if self.serveur: #si quelque chose dedans
            self.cadre=self.cadre+1
            if self.cadre==1:
                self.vue.centrerPlanete()
            self.modele.prochaineAction(self.cadre)
            self.vue.afficheArtefact()
            self.modele.chatSent=0
            self.vue.dataCadreInfosCiv()
            
            #Condition de defaite version Nico
            self.enVie = self.modele.civs[self.nom].suisJeVivant() 
            if self.enVie == 2:
                print("TES MORRRRRRT")
                self.deces()
                self.vue.initMort()
                
            else:
                print("en vie")
            
                if self.actions: #si quelque chose dedans
                    rep=self.serveur.faitAction([self.nom,self.cadre,self.actions])
                    self.actions=[] #on vide la liste, puisqu'elle a ete envoyee
                else:
                    rep=self.serveur.faitAction([self.nom,self.cadre,0])
                #print("OUT",self.cadre)
                #print("BACK")
                
                if rep[0]:
                    print("Serveur = ",rep)
                    #input("QUELQUE CHOSE SUR LE RADAR")
                    for i in rep[2]: #s'occupe de la chaine d'action
                        if i not in self.modele.actionsAFaire.keys():
                            self.modele.actionsAFaire[i]=[]
                            for k in rep[2][i]:
                                for m in k:
                                    self.modele.actionsAFaire[i].append(m)
                        #else:
                            #for k in rep[2][i]:
                                #for m in k:
                          ##                                      #self.modele.actionsAFaire[i]=[m]
                    print("ACTIONS",self.cadre,"\nREP",rep,"\nACTIONAFAIRE",self.modele.actionsAFaire)  
                if rep[1]=="attend": #au cas ou le frame etait plus en avance que le autres
                    self.cadre=self.cadre-1
    #                        if j[0]=="creerVaisseau" and j[1][0]==self.nom:
    #                            self.modele.vaisseau=Vaisseau(j[1])
    #                        elif j[0]=="#  QcreerVaisseau":
    #                            self.modele.vaisseaux.append(Vaisseau(j[1]))
    #                        elif j[0]=="changeCible":
    #                            self.modele.changerCible(j[1])
                if self.vue.perspectiveCouranteTexte=="Espace" or self.vue.perspectiveCouranteTexte=="Planete":
                    self.vue.verifieEcranBouge()
                    rep=self.serveur.faitAction([self.nom,self.cadre,0])
                #print("OUT",self.cadre)
                #print("BACK")
                
                if rep[0]:
                    print("Serveur = ",rep)
                    #input("QUELQUE CHOSE SUR LE RADAR")
                    for i in rep[2]: #s'occupe de la chaine d'action
                        if i not in self.modele.actionsAFaire.keys():
                            self.modele.actionsAFaire[i]=[]
                            for k in rep[2][i]:
                                for m in k:
                                    self.modele.actionsAFaire[i].append(m)
                        #else:
                            #for k in rep[2][i]:
                                #for m in k:
                                    #self.modele.actionsAFaire[i]=[m]
                    print("ACTIONS",self.cadre,"\nREP",rep,"\nACTIONAFAIRE",self.modele.actionsAFaire)  
                if rep[1]=="attend": #au cas ou le frame etait plus en avance que le autres
                    self.cadre=self.cadre-1
    #                        if j[0]=="creerVaisseau" and j[1][0]==self.nom:
    #                            self.modele.vaisseau=Vaisseau(j[1])
    #                        elif j[0]=="#  creerVaisseau":
    #                            self.modele.vaisseaux.append(Vaisseau(j[1]))
    #                        elif j[0]=="changeCible":
    #                            self.modele.changerCible(j[1])
                if self.vue.perspectiveCouranteTexte=="Espace" or self.vue.perspectiveCouranteTexte=="Planete":
                    self.vue.verifieEcranBouge()
                    
                self.vue.root.after(50,self.timerJeu)
               
                
        else:
            print("Aucun serveur connu")
            
    def drawLaser(self,obj1,obj2):
        self.deleteLaser(obj1)
        self.vue.drawLaser(obj1,obj2)
            
    def deleteLaser(self,obj):
        self.vue.deleteLaser(obj)
 
    def supprimer(self, o):
        self.modele.civs[self.nom].supprimer(o)
        
    def upgradeVitesse(self, k):
        self.modele.civs[self.nom].upgradeVitesse(k)
        
    def upgradePower(self, g):
        self.modele.civs[self.nom].upgradePower(g)
        
    def upgradeMinerais(self, m):
        self.modele.civs[self.nom].upgradeMinerais(m)
        
    def upGaz(self, d):
        self.modele.civs[self.nom].upGaz(d)
        
    def selectObject(self, selection):
        print("Select Object: ",self.nom)
        self.modele.selectObject(selection,self.nom)
        
if __name__ == '__main__':
    #s=input("1")
    c=Controleur()
    c.vue.root.mainloop()
    print("FIN")