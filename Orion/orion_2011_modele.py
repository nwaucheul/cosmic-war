# -*- encoding: ISO-8859-1 -*-

import random
import math
from threading import *
from helper import Helper


class Etoile(object):
    def __init__(self,parent,id,x,y):
        self.parent=parent
        self.id=id
        self.proprietaire=""
        self.station=None
        self.x=x
        self.y=y
        #self.taille=random.randrange(100)+10
        self.planetes=[]
        self.creePlanetes()
        
        
    def creePlanetes(self):
        #n=random.randrange(3) #Chaque etoile a une chance sur 3 de n'avoir aucune planete
        n=8
        if n>0: #Si l'etoile possede des planetes
            nbPlanetes=random.randrange(3,5) #Cree un nombre random de planetes
            noPlaneteSurEtoile=0
            for i in range(nbPlanetes): #Pour chaque planete
                couleurPlanete=random.randrange(3)
                taillePlanete=random.randrange(10,50) #Initialise la taille de la planete
                x,y=self.trouveXYPlanete(taillePlanete)
                self.planetes.append(Planete(self,x,y,taillePlanete,self.parent.couleursPlanete[couleurPlanete],noPlaneteSurEtoile)) #Ajoute la planete creee a la liste de planete
                noPlaneteSurEtoile+=1

                
    def creerBatiment(self, batiment):
        self.station=batiment
        print(batiment)
        

    def creerBatiment(self, batiment):
        self.station=batiment
        print(batiment)
        
    def trouveXYPlanete(self,taillePlanete):
        self.posValide=0
        while self.posValide==0:
            self.posValide=1
            x=random.randrange(0,self.parent.paramPartie["x_systeme"])
            y=random.randrange(0,self.parent.paramPartie["y_systeme"])
            #S'assure que la planete rentre ds le canevas
            if x-taillePlanete>0 and y-taillePlanete>0 and x+taillePlanete<self.parent.paramPartie["x_systeme"] and y+taillePlanete<self.parent.paramPartie["y_systeme"]:
                #S'assure qu'elle n'entre pas en collison avec une autre planete
                for i in self.planetes:
                    #Si c'est invalide
                    if (x-taillePlanete)<=(i.x+i.taille) and x>i.x :
                        self.posValide=0
                        break
                    elif (y-taillePlanete)<=(i.y+i.taille) and y>i.y:
                        self.posValide=0
                        break
                    elif (x+taillePlanete)>=(i.x-i.taille) and x<i.x:
                        self.posValide=0
                        break
                    elif (y+taillePlanete)>=(i.y-i.taille) and y<i.y:
                        self.posValide=0
                        break
                    else:
                        self.posValide=1
            else:
                self.posValide=0
        
        return x,y

    


class Planete(object):    
    def __init__(self,parent,x,y,t,couleur,noPlanete):
        self.parent=parent
        self.x=x
        self.y=y
        self.taille=t
        self.type="Planete"
        self.noPlanete=noPlanete
        self.couleur=couleur
        self.habitee=0
        self.gazPlanete = random.randrange(self.taille)*500
        self.minPlanete = random.randrange(self.taille)*500
        self.materiau=random.randrange(self.taille)*10000*self.parent.parent.decupMat #Initialise le materiau disponible
        self.energie=random.randrange(self.taille)*1000000*self.parent.parent.decupEner #Initialise l'energie disponible (a verifier)
        self.vivres=random.randrange(self.taille)*1000*self.parent.parent.decupViv #Initialise les vivres disponibles (a verifier)
        self.entite={"Batiments":[],
                     "UniteTerrestre":[]}
        self.vie=10000
        self.initVie=self.vie

    def creerBatiment(self, batiment):
        self.entite.get("Batiments").append(batiment)
        print(batiment)

    def creerUntiteTerrestre(self, uniteTerrestre):
        self.entite.get("UniteTerrestre").append(uniteTerrestre)
        print(uniteTerrestre)

class Civ(object):
    def __init__(self,parent,nom,etoileMere,planeteMere,couleur):
        self.parent=parent
        self.nom=nom
        self.planeteMere=planeteMere
        self.selection=[]
        self.listeChat=[]
        self.posDep=[]
        self.minerai = 1000
        self.gaz= 400
        self.etoileMere=etoileMere
        self.couleur=couleur
        self.planetesColonisees=[planeteMere]
        self.nbPlanetes = len(self.planetesColonisees)
        self.nbScout=0
        self.nbCorvette=0
        self.nbFregate=0
        self.nbDestroyer=0
        self.nbColonisateur=0
        self.nbCollectGaz=0
        self.nbCollectMin=0
        self.nbCommandCenter=0
        self.nbCosmoLab=0
        self.etoilesVisites=[etoileMere]
        self.artefactsDecouverts=[]
        self.flottes=[]
        self.batiment=[]
        
        self.vivant=1
        
        self.entites={"Station":[Station(self,Modele.nextId(),etoileMere)],
                      "UniteTerrestre":[],
                      "Vaisseaux":[],
                      "Batiments":[]
        }
        self.prix={"gazScout":0,
                   "minScout":50,
                   "gazCorvette":50,
                   "minCorvette":125,
                   "gazMothership":400,
                   "minMothership":500,
                   "gazFregate":125,
                   "minFregate":75,
                   "gazDestroyer":225,
                   "minDestroyer":300,
                   "gazCollecteurGaz":0,
                   "minCollecteurGaz":150,
                   "gazCollecteurMin":0,
                   "minCollecteurMin":150,
                   "gazCosmoLab":150,
                   "minCosmoLab":250,
                   "gazCommandCenter":0,
                   "minCommandCenter":500,
                   "gazStationOrbital":300,
                   "minStationOrbital":500
                   }

        self.creerBatimentTemp(self.etoilesVisites[0])
        print("Planete mere : ",planeteMere.noPlanete)

        for i in self.etoileMere.planetes:
            self.planetesColonisees.append(i)

        self.actions={"changeCible":self.changeCible,
                      "creerVaisseau":self.creerVaisseau,
                      "captureChat":self.captureChat,
                      "creerBatiment":self.creerBatiment,
                      "creerStation": self.creerStation,
                      "creerPeon":CommandCenter.creeUnite
        }

    def captureChat(self,arg):
        self.listeChat.append(arg[0])
        print ("captureChat ",self.listeChat)
        self.parent.chatSent=1

    def faitAction(self):
        f = self.actions["changeCible", "creerVaisseau","captureChat","creerStation", "creerBatiment"]
        f(par)


    def changeCible(self,par): #Ajoute un unite a la selection du joueur
        id,x,y=par
        print("Selection len: ",len(self.parent.vue.selection))
        cibleAttaque=self.viserEnnemi([x,y])
        print(cibleAttaque)
        for i in self.entites["Vaisseaux"]: #Il n'y a pas ma selection dans le modele de l'autre [SYNC ISSUE]
        #EDIT: Tous les vaisseaux bougent en meme temps maintenant [SELECTION ISSUE]
            if id==i.id:
                if isinstance(i,Vaisseau):
                    i.changeCible(x,y)
                    if cibleAttaque:
                        i.ennemi=cibleAttaque
                    print("CHANGER VAISSEAU CIBLE")
                    
    def supprimer(self, o):
        for i in self.entites.keys():
            print(i)
            if i == o.tag:
                for j in self.entites[i]:
                    if (int(j.id))==(int(o.id)):
                        self.entites[i].remove(j)
                        print("supprime")
                        
    def suisJeVivant(self):        
        compteur=0
        for i in self.entites["Station"]:
            compteur=compteur+1
            return 1
        if compteur==0:
            print("je suis mort :(")
            return 2
            
            
        


    def upgradeVitesse(self, k):
        for i in self.entites.keys():
            if i == k.tag:
                for j in self.entites[i]:
                    print(j)
                    if (int(j.id))==(int(k.id)):
                        j.vitesse+=5
                        print("UPGRADE VITESSE")
                        
    def upgradePower(self, g):
        for i in self.entites.keys():
            if i == g.tag:
                for j in self.entites[i]:
                    print(j)
                    if (int(j.id))==(int(g.id)):
                        j.force+=5
                        print("UPGRADE ATTACK")
                        
    def upgradeMinerais(self, m):
        for i in self.entites.keys():
            if i == m.tag:
                for j in self.entites[i]:
                    print(j)
                    if (int(j.id))==(int(m.id)):
                        j.niveauMin+=1
                        print("UPGRADE Minerais")
                        
    def upGaz(self, d):
        for i in self.entites.keys():
            if i == d.tag:
                for j in self.entites[i]:
                    print(j)
                    if (int(j.id))==(int(d.id)):
                        j.niveauGaz+=1
                        print("UPGRADE GAZ")
                        
    def prochaineAction(self): #Assigne la prochaine action a effectuer sur le frame disponible suivant (A verifier)
        #print("Frame: ",self.parent.parent.cadre)
        #print(self.parent.parent.vue.perspectiveCourante)
        for i in self.entites.keys():
            for j in self.entites[i]:
                j.prochaineAction()

    def evalueAction(self):
        for i in self.entites.keys():
            for j in self.entites[i]:
                x=34*3/4*5+9
        for i in self.entites.keys():
            for j in self.entites[i]:
                x=34*3/4*5+9

    def creerVaisseau(self,par):
        type,id=par
        x=self.etoilesVisites[0].x
        y=self.etoilesVisites[0].y

        j=self.entites["Station"]
        for i in j:
            if i.id == id:
                x=i.x
                y=i.y
                self.etoile=i
                break
        print("Position initial",x,y)
        self.pos=[x,y]
        self.x=self.pos[0]
        self.y=self.pos[1]


        if type=="Scout":
            if self.gaz>=self.prix["gazScout"]:
                if self.minerai >= self.prix["minScout"]:
                    temp = Scout(self, Modele.nextId(),self.etoile, self.pos,5) #Cree le vaisseau
                    self.entites.get("Vaisseaux").append(temp)
                    #self.nbScout=len(self.entites.get("Vaisseaux"))  #nombre de scouts
                    self.gaz -= self.prix["gazScout"]
                    self.minerai -= self.prix["minScout"]
                    self.nbScout=self.nbScout+1
                    print ("Scout Cr�� !")
        elif type=="Corvette":
            if self.gaz >= self.prix["gazCorvette"]:
                if self.minerai > self.prix["minCorvette"]:
                    temp = Corvette(self, Modele.nextId(),self.etoile, self.pos,5)
                    self.entites.get("Vaisseaux").append(temp)
                    self.gaz -= self.prix["gazCorvette"]
                    self.minerai -= self.prix["minCorvette"]
                    self.nbCorvette=self.nbCorvette+1
                    print ("Corvette Cr�� !")
        elif type=="Colonisateur":
            if self.gaz >= self.prix["gazMothership"]:
                if self.minerai > self.prix["minMothership"]:
                    temp = Colonisateur(self, Modele.nextId(),self.etoile, self.pos,5)
                    self.entites.get("Vaisseaux").append(temp)
                    #self.nbColonisateur=len(self.entites.get("Vaisseaux").append(temp))  #nombre de colonisateurs
                    self.gaz -= self.prix["gazMothership"]
                    self.minerai -= self.prix["minMothership"]
                    self.nbColonisateur=self.nbColonisateur+1
                    print ("Colonisateur Cr�� !")
        elif type=="Fregate":
            if self.gaz >= self.prix["gazFregate"]:
                if self.minerai > self.prix["minFregate"]:
                    temp = Fregate(self, Modele.nextId(),self.etoile, self.pos,5)
                    self.entites.get("Vaisseaux").append(temp)
                    self.gaz -= self.prix["gazFregate"]
                    self.minerai -= self.prix["minFregate"]
                    self.nbFregate=self.nbFregate+1
                    print ("Fregate Cr�� ! Faites attention!")
        elif type=="Destroyer":
            if self.gaz >= self.prix["gazDestroyer"]:
                if self.minerai > self.prix["minDestroyer"]:
                    temp = Destroyer(self, Modele.nextId(),self.etoile, self.pos,5)
                    self.entites.get("Vaisseaux").append(temp)
                    self.gaz -= self.prix["gazDestroyer"]
                    self.minerai -= self.prix["minDestroyer"]
                    self.nbDestroyer=self.nbDestroyer+1
                    print ("Destroyer Cr�� ! Pure madness")
        else:
            print("Pas assez de ressources")
                

    def creerBatiment(self,par):
        type,noPlanete,idEtoile=par
        self.pos=[0,0]
        planete=None
        print(type)

        for i in self.planetesColonisees:
            print("Planete selectionee :",noPlanete,"Planete colonise",i.noPlanete)
            if noPlanete == i.noPlanete:
                self.pos[0]=i.x
                self.pos[1]=i.y
                planete=i
                break
            else:
                print("Planete non colonisee")

        if type=="CollecteurGaz":
            if self.gaz >= self.prix["gazCollecteurGaz"]:
                if self.minerai >= self.prix["minCollecteurGaz"]:
                    temp = CollecteurGaz(self, self.pos, Modele.nextId(), noPlanete,idEtoile,5) #Cree le batiment
                    self.entites.get("Batiments").append(temp)
                    planete.entite.get("Batiments").append(temp)        
                    self.gaz -= self.prix["gazCollecteurGaz"]
                    self.minerai -= self.prix["minCollecteurGaz"]
                    self.nbCollectGaz=self.nbCollectGaz+1
                    print("Collecteur de gaz Cr��", temp)

        elif type=="CollecteurMinerais":
            if self.gaz >= self.prix["gazCollecteurMin"]:
                if self.minerai >= self.prix["minCollecteurMin"]:
                    temp = CollecteurMinerais(self, self.pos, Modele.nextId(), noPlanete,idEtoile,5) #Cree le batiment
                    self.entites.get("Batiments").append(temp)
                    planete.entite.get("Batiments").append(temp)
                    self.gaz -= self.prix["gazCollecteurMin"]
                    self.minerai -= self.prix["minCollecteurMin"]
                    self.nbCollectMin=self.nbCollectMin+1
                    print("Collecteur de minerais Cr��", temp)
        elif type=="Cosmolab":
            if self.gaz >= self.prix["gazCosmoLab"]:
                if self.minerai >= self.prix["minCosmoLab"]:
                    temp = Cosmolab(self, self.pos, Modele.nextId(), noPlanete,idEtoile,5)
                    self.entites.get("Batiments").append(temp)
                    planete.entite.get("Batiments").append(temp)
                    self.gaz -= self.prix["gazCosmoLab"]
                    self.minerai -= self.prix["minCosmoLab"]
                    self.nbCosmoLab=self.nbCosmoLab+1
                    print("Cosmolab Cr��", type)
        elif type=="CommandCenter":
            if self.gaz >= self.prix["gazCommandCenter"]:
                if self.minerai >= self.prix["minCommandCenter"]:
                    temp = CommandCenter(self, self.pos, Modele.nextId(), noPlanete,idEtoile,5)
                    self.entites.get("Batiments").append(temp)
                    planete.entite.get("Batiments").append(temp)
                    self.gaz -= self.prix["gazCommandCenter"]
                    self.minerai -= self.prix["minCommandCenter"]
                    self.nbCommandCenter=self.nbCommandCenter+1
                    print("idEtoile",idEtoile)
                    print("idPlanete",noPlanete)
                    print(len(self.parent.etoiles))
                    print(len(self.parent.etoiles[idEtoile].planetes))
                    self.parent.etoiles[idEtoile].planetes[noPlanete].habitee=1
                    print("CommandCenter Cr��", type)
        else:
            print("Pas assz de ressources")

    def creerStation(self,par):
        id=par
        idS = id[0]
        for i in self.etoilesVisites:
            print("Etoile visitees :",i.id,"Etoile selectionnee :",idS)
            if idS == i.id:
                etoile=i
                break
            else:
                print("Etoile non acquise")
        if self.gaz >= self.prix["gazStationOrbital"]:
            if self.minerai >= self.prix["minStationOrbital"]:
                temp = Station(self, Modele.nextId(),etoile)
                self.entites.get("Station").append(temp)
                print(self.entites.get("Station")[1])
                self.gaz -= self.prix["gazStationOrbital"]
                self.minerai -= self.prix["minStationOrbital"]
                print("Station Cr��")
            else:
                print("Sale pauvre")
        else:
            print("Sale pauvre")

    def creerUniteTerrestre(self, planete, type, pos):
        if type=="Ouvrier":
            pos[0]=batiment.x
            pos[1]=batiment.y
            temp = Ouvrier(self, )
        self.planeteColonisees.get(planete).creerUniteTerrestre(uniteTerrestre) #Assigne l'unite a sa planete

    def creerBatimentTemp(self, etoile):
        temp = Station(self, Modele.nextId(), etoile)#Cree le batiment
        for i in self.etoilesVisites:
            if i==etoile:
                i.creerBatiment(temp) #Assigne le batiment a sa planete
    
    #detecte si on peut attaquer l'ennemi
    def viserEnnemi(self, pos):
        x=pos[0]
        y=pos[1]
        cibleAttaque=None
        print("VISE LENNEMI")

        for i in self.parent.civs.keys():
            #si la cible ns appartient pas
            if self.parent.civs[i].nom!=self.nom:
                for j in self.parent.civs[i].entites.keys():
                    for k in self.parent.civs[i].entites[j]:
                        if k.x<x+10:
                            if k.x>x-10:
                                if k.y<y+10:
                                    if k.y>y-10:
                                        print("Its something")
                                        cibleAttaque=k
        return cibleAttaque
    


class Unite(object):
    def __init__(self, parent, pos, id, planete):
        self.parent = parent
        self.x = pos[0]
        self.y = pos[1]
        self.id = id
        self.angle=0
        self.cible=[]
        self.planete=planete
        self.armureLvl=0
        self.attackLvl=0

    def deplacer(self):
        if self.cible:
            x, y=Helper.getAngledPoint(self.angle,self.vitesse,self.x,self.y)
            self.x = x
            self.y = y

        d=Helper.calcDistance(self.x,self.y,self.cible[0],self.cible[1])
        if d < self.vitesse:
            self.cible=[]

    def prochaineAction(self):
        self.deplacer()

    def changeCible(self,x,y):
        self.cible=[x,y]
        self.angle=Helper.calcAngle(self.x,self.y,self.cible[0],self.cible[1])


class Ouvrier(Unite): #sous classe d'unite
    def __init__(self, parent, pos, id):
        self.vitesse = 3
        Unite.__init__(self, parent, pos, id, planete)
        bOuvrier= Buttonjm(self.cadreNiveauVue,text="Ouvrier")
        bOuvrier.grid(column=0,row=0,sticky=E+W)

    def construire(self): #Construire des batiments
        pass

    def populer(self): #Creer d'autre personne
        self.unitee.get("Ouvrier").append(Ouvrier) #va aller chercher "ouvrier" dans le dictionnaire
        self.max = 10 #populer 10 personnes max.

    def deplacer(self):
        if self.cible:
            x, y=Helper.getAngledPoint(self.angle,self.vitesse,self.x,self.y)
            self.x = x
            self.y = y
            d=Helper.calcDistance(self.x,self.y,self.cible[0],self.cible[1])
            if d < self.vitesse:
                self.cible=[]

    def prochaineAction(self):
        self.deplacer()

class Batiment(object):
    def __init__(self,parent, position, id, noPlanete, idEtoile,tag="Batiments"):
        self.parent=parent
        self.idPlanete=noPlanete
        self.idEtoile=idEtoile
        self.tag=tag
        self.id=id
        self.niveau=1
        self.niveauGaz=1
        self.niveauMin=1
        self.armureLvl=0
        self.attackLvl=0

class CollecteurGaz(Batiment):
    def __init__(self, parent, position, id, noPlanete, idEtoile, tag):
        Batiment.__init__(self, parent, position, id, noPlanete, idEtoile)
        self.population = 0
        self.vie=500
        self.noPlanete=noPlanete
        self.modulo =0
        self.recolte =10
        self.quantiteGaz = 0
        self.initVie=self.vie
        self.gazPlanete=0
        self.minPlanete=0
        self.vitesse = 25
        self.type="CollecteurGaz"
        self.x=position[0]-10-(len(self.parent.entites.get("Batiments"))*10)
        self.y=position[1]-10-(len(self.parent.entites.get("Batiments"))*10)

    def collecterGaz(self):
        for i in self.parent.planetesColonisees:
            if i.noPlanete == self.noPlanete:
                if i.gazPlanete > 0:
                    self.quantiteGaz = self.recolte+ self.niveauGaz*(self.recolte*0.10)+self.population*(self.recolte*0.10)
                    self.parent.gaz += int(self.quantiteGaz)
                    i.gazPlanete = i.gazPlanete - self.quantiteGaz
                else:
                    print("il n'y a plus de gaz sur la planete!")
        
    def populerGaz(self):
        self.population += 1

    def prochaineAction(self):
        if self.modulo %self.vitesse == 0 :
            self.collecterGaz()
            print("GAZ COLLECTE")
            print(self.quantiteGaz)
            print("TOTAL GAZ")
            print (self.parent.gaz) 
        self.modulo += 1

class CollecteurMinerais(Batiment):
    def __init__(self, parent, position, id, noPlanete, idEtoile, tag):
        Batiment.__init__(self, parent, position, id, noPlanete, idEtoile)
        self.population = 0
        self.vie=500
        self.noPlanete=noPlanete
        self.modulo =0
        self.recolte = 10
        self.quantiteMin = 0
        self.initVie=self.vie
        self.gazPlanete=0
        self.minPlanete=0
        self.vitesse=25
        self.type="CollecteurMinerais"
        self.x=position[0]-10-(len(self.parent.entites.get("Batiments"))*10)
        self.y=position[1]-10-(len(self.parent.entites.get("Batiments"))*10)

    def collecterMinerai(self):
        for i in self.parent.planetesColonisees:
            if i.noPlanete == self.noPlanete:
                if i.minPlanete > 0:
                    self.quantiteMin = self.recolte+ self.niveauMin*(self.recolte*0.10)+self.population*(self.recolte*0.10)
                    self.parent.minerai += int(self.quantiteMin)
                    i.minPlanete = i.minPlanete - self.quantiteMin
                else:
                    print("il n'y a plus de minerai sur la planete!")
        
    def populerMinerai(self):
        self.population += 1

    def prochaineAction(self):
        if self.modulo %self.vitesse == 0 :
            self.collecterMinerai()
            print("MINERAI COLLECTE")
            print(self.quantiteMin)
            print("TOTAL MINERAI")
            print (self.parent.minerai)
            
        self.modulo += 1

class Cosmolab(Batiment):
    def __init__(self,parent, position, id, noPlanete, idEtoile, tag):
        Batiment.__init__(self,parent, position, id, noPlanete, idEtoile)
        self.vitesse = (self.niveau*1.2)*10
        self.type="Cosmolab"
        self.upgrade=None
        self.initVie=100
        self.gazPlanete=0
        self.minPlanete=0
        self.force=1
        self.noPlanete=noPlanete
        self.x=position[0]-10-(len(self.parent.entites.get("Batiments"))*10)
        self.y=position[1]+10-(len(self.parent.entites.get("Batiments"))*10)

    def prochaineAction(self):
        pass
                        

class CommandCenter(Batiment):
    def __init__(self,parent, position, id, noPlanete, idEtoile, tag):
        Batiment.__init__(self,parent, position, id, noPlanete, idEtoile)
        self.vitesse = (self.niveau*1.2)*10     # Temps entre chaque creation
        self.tabCitoyen=[]                      # Liste d'attente de creation
        self.vie=2500
        self.noPlanete=noPlanete
        self.type="CommandCenter"
        self.x=position[0]
        self.y=position[1]
        self.initVie=self.vie
        self.gazPlanete=0
        self.minPlanete=0
        

    def creeUnite(self,par):   # Ajout du vaisseau de l'emplacement 0 de la liste d'attente
    # dans le tableau de vaisseau de la civilisation, tout les "self.vitesse"
        t = Timer(self.vitesse,self.tabCitoyen.append(par)).start()

    def prochaineAction(self):
        if self.tabCitoyen:
            self.parent.Civ.creerVaisseau(self.tabCitoyen[0])
            self.tabCitoyen.remove(self.tabCitoyen[0])


class Station(object):
    def __init__(self,parent, id, etoile):
        self.parent=parent
        self.id=id
        self.type="station"
        self.nom=parent.nom
        #self.demarage=demarage
        self.niveau=1
        self.vitesse = (self.niveau*1.2)*10     # Temps entre chaque creation
        self.initVie=1500
        self.vie=self.initVie
        #On trouve soit -1 ou 1 pour dirx et diry de maniere aleatoire
        self.armureLvl=0
        self.attackLvl=0

        dirx=random.randrange(2)-1
        if dirx==0:
            dirx=1
        diry=random.randrange(2)-1
        if diry==0:
            diry=1
            #On place la station aleatoirement par rapport a l'etoile mere
        self.x=(random.randrange(10)+10*dirx)+etoile.x
        self.y=(random.randrange(10)+10*diry)+etoile.y

        print("STATION",self.x,self.y)

    def creeUnite(self,par):   # Ajout du vaisseau de l'emplacement 0 de la liste d'attente
    # dans le tableau de vaisseau de la civilisation, tout les "self.vitesse"
        pass

    def prochaineAction(self):
        pass
    
    def perdsVie(self,pts):
        self.vie=self.vie-pts
        reponse=False
        if self.vie <=0:
            #self.detruire()
            if self in self.parent.entites["Station"]:
                self.parent.entites["Station"].remove(self)
            if not self.parent.entites["Station"]:
                self.parent.vivant=0
                print("Aaaannnnddd im dead")
            reponse=True
        return reponse

    
class Vaisseau(object):
    def __init__(self,parent,id,pos,tag="Vaisseaux"):
        self.parent=parent
        self.id=id
        self.pos=pos
        self.tag=tag
        self.couleur=parent.couleur
        #self.carburant = 100     Nico : Idee pour plus tard
        self.nom=parent.nom
        self.deplacePourAttaque = False
        #on place le premier vaisseau initial 10 x a droite de letoile mere
        #puis les autres a 10x de chaque
        self.angle=0
        self.cible=[]
        self.ennemi=None
        self.modulo = 1000
        self.armureLvl=0
        self.attackLvl=0
        
    def changeCible(self,x,y):
        self.cible=[x,y]
        self.angle=Helper.calcAngle(self.x,self.y,self.cible[0],self.cible[1])
        #for (self.parent.)
        #print("CALC",[x,y])
    
    def perdsVie(self,pts):
        self.vie=self.vie-pts
        reponse=False
        if self.vie <=0:
            self.detruire()
            #print("Aaaannnnddd im dead")
            reponse=True
            self.parent.parent.parent.deleteLaser(self)
        return reponse
            
    def detruire(self):
        for i in self.parent.entites["Vaisseaux"]:
            if i.id==self.id:
                self.parent.entites["Vaisseaux"].remove(i)
                if i in self.parent.selection:
                    self.parent.selection.remove(i)
        
    def bougerVaisseau(self):
        if self.ennemi:
            self.attaquer()
            
        if self.cible:
            #Avec le helper on trouve le nouveau x et y du vaisseau(on le deplace)
            x,y=Helper.seDeplaceAuPoint(self.angle,self.vitesse,self.x,self.y)
            self.x=x
            self.y=y
            #print("CALC2",[x,y])
            #On calcule la distance entre le vaisseau et la cible, si c'est
            #moins que la vitesse qu'il se deplace, il est arrive
            distance=Helper.calcDistance(self.x,self.y,self.cible[0],self.cible[1])
            if distance<self.vitesse:
                self.cible=[]
            if self.deplacePourAttaque:
                self.attaquer()
                
    def verifierRayon(self):
        #print("Verifying...")
        for i in self.parent.parent.civs.keys():
            if self.parent.parent.civs[i].nom!=self.nom:
                for j in self.parent.parent.civs[i].entites.keys():
                    if j!="Batiments":
                        for k in self.parent.parent.civs[i].entites[j]:
                            if k.x<self.x+self.rayon:
                                if k.x>self.x-self.rayon:
                                    if k.y<self.y+self.rayon:
                                        if k.y>self.y-self.rayon:
                                            #print("There's an enemy!")
                                            self.ennemi=k
    
    #methode attaquer qui sera disponible pour chaque type de vaisseau, mais avec differentes forces
    def attaquer(self):
        if self.inRange():
            self.parent.parent.parent.drawLaser(self,self.ennemi)
            self.cible=[]
            #print("IN RANGE")
            self.tir = random.randrange(int (self.force-(self.force*0.2)),int(self.force+(self.force*0.2)))
            isDead=self.ennemi.perdsVie(self.tir)
            #print(self.ennemi.vie)
            if isDead:
                self.parent.parent.parent.deleteLaser(self)
                self.ennemi=None
        else:
            self.parent.parent.parent.deleteLaser(self)
            
    def prochaineAction(self):
        self.bougerVaisseau()
        if self.ennemi==None:
            self.verifierRayon()
        else:
            if self.modulo %20 == 0 :
                self.attaquer()
            self.modulo += 1
        
    def inRange(self):
        e=self.ennemi
        reponse=False
        if e.x<self.x+self.rayon:
            if e.x>self.x-self.rayon:
                if e.y<self.y+self.rayon:
                    if e.y>self.y-self.rayon:
                        reponse=True
        return reponse
        
class Scout(Vaisseau):
    def __init__(self,parent,id,planete,pos,tag):
        Vaisseau.__init__(self,parent,id,planete)
        self.type="Scout"
        self.vitesse=10
        self.initVie=50
        self.vie=self.initVie
        self.force = 2
        self.rayon=125
        self.x=pos[0]+10+(len(parent.entites.get("Vaisseaux"))*10)
        self.y=pos[1]+15
        self.grosseur=10

        
class Corvette(Vaisseau):
    def __init__(self,parent,id,planete,pos,tag):
        Vaisseau.__init__(self,parent,id,planete)
        self.type="Corvette"
        self.vitesse=5
        self.initVie=100
        self.vie=self.initVie
        self.force = 10
        self.rayon=125
        self.x=pos[0]+10+(len(parent.entites.get("Vaisseaux"))*10)
        self.y=pos[1]+15
        self.grosseur=7

        
class Colonisateur(Vaisseau):
    def __init__(self,parent,id,planete,pos,tag):
        Vaisseau.__init__(self,parent,id,planete)
        self.type="Colonisateur"
        self.vitesse = 3
        self.vie = 2000
        self.pos=pos
        self.initVie=200
        self.vie=self.initVie
        self.force = 2
        self.rayon=50
        self.x=pos[0]+10+(len(parent.entites.get("Vaisseaux"))*10)
        self.y=pos[1]+15
        self.grosseur=12
        
class Fregate(Vaisseau):
    def __init__(self,parent,id,planete,pos,tag):
        Vaisseau.__init__(self,parent,id,planete)
        self.type="Fregate"
        self.vitesse = 10
        self.vie = 200
        self.initVie=20
        self.vie=self.initVie
        self.force = 12
        self.rayon=10
        self.x=pos[0]+10+(len(parent.entites.get("Vaisseaux"))*10)
        self.y=pos[1]+15
        self.grosseur=5
        
        
class Destroyer(Vaisseau):
    def __init__(self,parent,id,planete,pos,tag):
        Vaisseau.__init__(self,parent,id,planete)
        self.type="Destroyer"
        self.vitesse = 10
        self.vie = 1000
        self.initVie=200
        self.vie=self.initVie
        self.force = 20

        self.rayon=100
        self.x=pos[0]+10+(len(parent.entites.get("Vaisseaux"))*10)
        self.y=pos[1]+15
        self.grosseur=100

                          
class Modele(object):
    id=-1
    def nextId():
        Modele.id=Modele.id+1
        return Modele.id
    
    def __init__(self,parent):
        self.parent=parent
        self.paramPartie={"largeurFenetreJeu":8000,
                          "hauteurFenetreJeu":6000,
                          "x_espace":8000,
                          "y_espace":6000,
                          "x_systeme":800,
                          "y_systeme":600,
                          "x_planete":1000,
                          "y_planete":1000,
                          "Etoiles":20,
                          "max_planetes":10,
                          "min_materiau":10000,         # changement de minerai pour materiau
                          "max_materiau":10000,
                          "min_energie":10000,          # aucun changement
                          "max_energie":10000,
                          "min_vivre":1000,             # ajout de vivre pour les ouvriers
                          "min_vivre":1000
        }
        self.decupMat=1                 # ajout eventuel de decupleurs de ressources pour les trois types
        self.decupEner=1                # pour refleter un certain niveau de developement
        self.decupViv=1                   
        self.rdseed=0                                                           
        self.civs={}
        self.vaisseau=""
        self.vaisseaux=[]
        self.actions=[]
        self.actionsAFaire={}
        self.etoiles=[]
        self.couleursPlanete=["red","blue","green"]
        self.chatSent=0
        
    def initPartie(self,listeNomsJoueurs):
        self.creerCarresMap()
        self.listeCasesDispos=[0,1,2,3,4,5,6,7]
        self.listeCasesOccupees=[]
        self.vue=self.parent.vue
        random.seed(self.rdseed) #assigne un numero de random au modele pour que chaque joueur ait le meme random
        
        #on cree toutes les etoiles en leur assignant une
        #position xy et un id. On les ajoute ensuite a la liste
        #des etoiles du modele
        for i in range(self.paramPartie["Etoiles"]):
            x=random.randrange(self.paramPartie["x_espace"])
            y=random.randrange(self.paramPartie["y_espace"])
            id=Modele.nextId()
            self.etoiles.append(Etoile(self,id,x,y))
        
        
        couleurs=["red","blue","green","yellow","orange","purple","grey","black"]
        nbJoueurs=0
        
        #Cree chaque joueur(joueur=civilisation) en leur
        #assignant id,position xy. Cree leur etoile mere.
        ######################################AI###############################################
        listeNomsJoueurs.append("Bot_1")
        ######################################################################################
        
        for j in listeNomsJoueurs:
            print(listeNomsJoueurs)
            x,y=self.trouveXYEtoileMere(len(listeNomsJoueurs),nbJoueurs+1)
            id=Modele.nextId()
            etoileMereNonTrouvee=1
            #Cree 1 etoile tant qu'elle n'a pas 0 planetes,
            #nb de planetes etant initialisee ak un random
            #dans init de la classe Etoile
            while etoileMereNonTrouvee:
                etoileMere=Etoile(self,id,x,y)
                if len(etoileMere.planetes):
                    etoileMereNonTrouvee=0
            print("NOM:",j)
            self.nom=j
            
            #Selectionne une planete mere dans l'etoile mere        
            planeteMere=etoileMere.planetes[random.randrange(len(etoileMere.planetes))]
            self.etoiles.append(etoileMere)
            if j.split("_", 1)[0]=="Bot":
                print("Creating bot")
                self.civs[j]=Bot(self,j,etoileMere,planeteMere,couleurs[nbJoueurs])
                """
                par=["CommandCenter",planeteMere.noPlanete,etoileMere.id]
                self.civs[j].creerBatiment(par)
                n=7
                x=100
                y=100
                coul=self.civs[j].couleur
                self.vue.canevasPlanete.create_oval(x-n,y-n,x+n+35,y+n+35,outline=coul,fill="black",tags=("artefact",i,id,"Batiments"))
                self.vue.canevasPlanete.create_oval(x-n+10,y-n+10,x+n+25,y+n+25,outline=coul,fill="red",tags=("artefact",i,id,"Batiments"))
                self.vue.canevasPlanete.create_oval(x-n+13,y-n+13,x+n+22,y+n+22,outline=coul,fill="yellow",tags=("artefact",i,id,"Batiments"))
                print("commandcenter cree!!!!!!!!!!!!!!!!")
                """
            else:
                self.civs[j]=Civ(self,j,etoileMere,planeteMere,couleurs[nbJoueurs])
                """
                par=["CommandCenter",planeteMere.noPlanete,etoileMere.id]
                self.civs[j].creerBatiment(par)
                n=7
                x=100
                y=100
                coul=self.civs[j].couleur
                self.vue.canevasPlanete.create_oval(x-n,y-n,x+n+35,y+n+35,outline=coul,fill="black",tags=("artefact",i,id,"Batiments"))
                self.vue.canevasPlanete.create_oval(x-n+10,y-n+10,x+n+25,y+n+25,outline=coul,fill="red",tags=("artefact",i,id,"Batiments"))
                self.vue.canevasPlanete.create_oval(x-n+13,y-n+13,x+n+22,y+n+22,outline=coul,fill="yellow",tags=("artefact",i,id,"Batiments"))
                print("commandcenter cree!!!!!!!!!!!!!!!!")
                """
            nbJoueurs=nbJoueurs+1
            
    
    def creerCarresMap(self):
        #on separe le canevas en 8 cases, chaque a l comme largeur et h comme hauteur
        l=self.paramPartie["x_espace"]/4
        h=self.paramPartie["y_espace"]/2
        self.carresMap=[]
       
       
        for i in range(8):
            if(i<4):
                self.carresMap.append([i*l,(i+1)*l,0,h])
            else:
                self.carresMap.append([(i-4)*l,((i-4)+1)*l,h,h*2])
          
    def trouveXYEtoileMere(self,nombreDeJoueurs,noJoueur):
        self.case=random.choice(self.listeCasesDispos)
        if(nombreDeJoueurs==1):
            self.listeCasesDispos.remove(self.case)
            x=random.randrange(self.carresMap[self.case][0],self.carresMap[self.case][1])
            y=random.randrange(self.carresMap[self.case][2],self.carresMap[self.case][3])
            self.listeCasesOccupees.append(self.case)
            return x,y
        elif(nombreDeJoueurs==2):#a 2 joueurs on veut les 2 sur des lignes differentes
            if len(self.listeCasesOccupees)!=0:
                if self.listeCasesOccupees[0]<4 :
                    while self.case in self.listeCasesOccupees or self.case<4 or self.case-1 in self.listeCasesOccupees or self.case+1 in self.listeCasesOccupees or self.case+4 in self.listeCasesOccupees or self.case-4 in self.listeCasesOccupees:
                        self.case=random.choice(self.listeCasesDispos)
                else:
                    while self.case>4 or self.case-1 in self.listeCasesOccupees or self.case+1 in self.listeCasesOccupees or self.case+4 in self.listeCasesOccupees or self.case-4 in self.listeCasesOccupees:
                        self.case=random.choice(self.listeCasesDispos)
            x=random.randrange(self.carresMap[self.case][0],self.carresMap[self.case][1])
            y=random.randrange(self.carresMap[self.case][2],self.carresMap[self.case][3])
            self.listeCasesOccupees.append(self.case)
            return x,y
        elif(nombreDeJoueurs<5):#tant que ya 4 joueurs on ne doit pas avoir 2 joueurs cote a cote
            while self.case in self.listeCasesOccupees or self.case-1 in self.listeCasesOccupees or self.case+1 in self.listeCasesOccupees or self.case+4 in self.listeCasesOccupees or self.case-4 in self.listeCasesOccupees:
                self.case=random.choice(self.listeCasesDispos)
            x=random.randrange(self.carresMap[self.case][0],self.carresMap[self.case][1])
            y=random.randrange(self.carresMap[self.case][2],self.carresMap[self.case][3])
            self.listeCasesOccupees.append(self.case)
            return x,y
        else:
            while self.case in self.listeCasesOccupees:
                self.case=random.choice(self.listeCasesDispos)
            x=random.randrange(self.carresMap[self.case][0],self.carresMap[self.case][1])
            y=random.randrange(self.carresMap[self.case][2],self.carresMap[self.case][3])
            self.listeCasesOccupees.append(self.case)
            return x,y
        

       
    def selectObject(self, selection, nom):
        self.civs[nom].selection=selection
        print(self.civs[nom].selection)
    
    def prochaineAction(self,frame):
        if frame in self.actionsAFaire:
            for i in self.actionsAFaire[frame]:
                #print("nathael",i)
                if i [1]=="captureChat":
                    for j in self.civs.keys():
                        self.civs[j].actions[i[1]](i[2])
                else:
                    self.civs[i[0]].actions[i[1]](i[2])
                    
                    
                #print("Nom civ: ", i[0])
            del self.actionsAFaire[frame]
            #print("NO1",frame)
        for i in self.civs.keys():
            self.civs[i].prochaineAction()
            
        for i in self.civs.keys():
            self.civs[i].evalueAction()

            
class Bot(Civ):
    def __init__(self,parent,nom,etoileMere,planeteMere,couleur):
        Civ.__init__(self,parent,nom,etoileMere,planeteMere,couleur)
        print("Planete bot: ", planeteMere.x,planeteMere.y)
        self.etoile=etoileMere
        #for i in range(random.randrange(10)+10):
            #self.creerVaisseau(["Scout",self.etoilesVisites[0].id])
        self.nbStation=1
        self.nbCollecteurMinerais=0
        self.nbCollecteurGaz=0
        self.prochaineDecision=0
        self.aggro=random.randrange(10)+5
        

        
    def prochaineAction(self):
        self.decideAction()
        for i in self.entites.keys():
            for j in self.entites[i]:
                j.prochaineAction()
                
    def decideAction(self):
        if self.prochaineDecision==0:
            print("Bot minerais: ",self.minerai)
            print("Bot gaz: ",self.gaz)
            print(self.nbScout)
            if self.nbCollecteurMinerais==0:
                if self.prix.get("minCollecteurMin")<=self.minerai:
                    print("Creer collecteur minerais")
                    self.creerBatiment(["CollecteurMinerais",0,self.etoile.id])
                    self.nbCollecteurMinerais=self.nbCollecteurMinerais+1
            elif self.nbCollecteurGaz==0:
                if self.prix.get("minCollecteurGaz")<=self.minerai:
                    print("Creer collecteur gaz")
                    self.creerBatiment(["CollecteurGaz",0,self.etoile.id])
                    self.nbCollecteurGaz=self.nbCollecteurGaz+1
                else:
                    if self.prix.get("minScout")<=self.minerai:
                        print("Creer Scout")
                        self.creerVaisseau(["Scout",self.etoilesVisites[0].id])
                        self.nbScout=self.nbScout+1
                    else:
                        print("Bouger vaisseaux")
                        self.bougerFlotte()
            else:
                ran=random.randrange(100)
                if ran<50:
                    if self.prix.get("minScout")<=self.minerai:
                        print("Creer Scout")
                        self.creerVaisseau(["Scout",self.etoilesVisites[0].id])
                        self.nbScout=self.nbScout+1
                    else:
                        print("Bouger vaisseaux")
                        self.bougerFlotte()
                else:
                    if self.minerai<self.prix.get("minCorvette"):
                        print("Bouger vaisseaux")
                        self.bougerFlotte()
                    else:
                        if self.gaz>self.prix.get("gazCorvette"):
                            print("Cr�er Corvette")
                            self.creerVaisseau(["Corvette",self.etoilesVisites[0].id])
                        else:
                            print("Bouger vaisseaux")
                            self.bougerFlotte()
                        
            self.prochaineDecision=random.randrange(10)+20
        else:
            self.prochaineDecision=self.prochaineDecision-1
        
    def bougerFlotte(self):
        for i in self.entites.keys():
            if i=="Vaisseaux":
                for j in self.entites[i]:
                    self.bougerVaisseau(j)
                    
    def bougerVaisseau(self, vaisseau):
        if vaisseau.cible:
            print("Change cible")
            self.changerCibleVaisseau(vaisseau)
        else:
            self.changerCibleVaisseau(vaisseau)
                
    def changerCibleVaisseau(self, vaisseau):
        x=vaisseau.x+random.randrange(200)-random.randrange(200)
        y=vaisseau.y+random.randrange(200)-random.randrange(200)
        if x<1:
            x=x+100
        if y<1:
            y=y+100
        if x>self.parent.paramPartie.get("x_espace"):
            x=x-100
        if y>self.parent.paramPartie.get("y_espace"):
            y=y-100
        vaisseau.changeCible(x,y)
        
