# -*- encoding: ISO-8859-1 -*-
from tkinter import *
from tkinter.font import *
import tkinter.simpledialog as sd
import tkinter.messagebox as mb
import random
from collections import OrderedDict


class Buttonjm(Button): #Modele de bouton pour qu'ils soient tous pareils au niveau du texte
    def __init__(self,parent,**kw):
        f=Font(size=7,slant="italic",weight="bold")
        kw["font"]=f
        kw["fg"]="orange"
        kw["bg"]="grey25"
        kw["relief"]="groove"
        Button.__init__(self,parent,**kw)

class Labeljm(Label): #Modele de label pour qu'ils soient tous pareils (texte sur la fenętre)
    def __init__(self,parent,**kw):
        f=Font(size=7)
        kw["font"]=f
        kw["fg"]="orange"
        kw["bg"]="grey25"
        Label.__init__(self,parent,**kw)
        

class Vue(object):
    perspective=["cosmos","espace","systeme","planete"]

    def __init__(self,parent,largeurFenetreJeu,hauteurFenetreJeu,x_espace,y_espace,x_planete,y_planete,x_systeme,y_systeme):
        self.parent=parent
        self.modele=self.parent.modele
        self.selection=[] #Objet selectionné par l'usager
        self.con=0
        self.largeurFenetreJeu=largeurFenetreJeu
        self.hauteurFenetreJeu=hauteurFenetreJeu
        self.scrollx=0
        self.scrolly=0
        self.posSourisXDansEcran=0
        self.posSourisYDansEcran=0
        self.posFenetreXDansEcran=0
        self.posFenetreYDansEcran=0
        self.x_espace=x_espace
        self.y_espace=y_espace
        self.x_planete=x_planete
        self.y_planete=y_planete
        self.x_systeme=x_systeme
        self.y_systeme=y_systeme
        self.x_cosmos=x_espace
        self.y_cosmos=y_espace
        self.root=Tk()
        self.textType = StringVar()
        self.textVie = StringVar()
        self.textKills = StringVar()
        self.textArmee = StringVar()
        self.textGaz = StringVar()
        self.textMinerai = StringVar()
        self.textType.set("             ")
        self.root.protocol('WM_DELETE_WINDOW', self.intercepteFermeture)
        self.root.overrideredirect(1)
        self.imageEtoile=PhotoImage(file='star.gif')
        self.imageScout=PhotoImage(file='scout.gif')
        self.imageCorvette=PhotoImage(file='corvette.gif')
        self.imageFregate=PhotoImage(file='fregate.gif')
        self.imageDestroyer=PhotoImage(file='destroyer.gif')
        self.imageColonisateur=PhotoImage(file='colonisateur.gif')
        self.imageCommandCenter=PhotoImage(file='mothership.gif')
        self.widthEcran=self.root.winfo_screenwidth()
        self.heightEcran=self.root.winfo_screenheight()
        self.root.geometry("%dx%d+0+0" % (self.widthEcran,self.heightEcran))
        self.root.bind("<Escape>",self.intercepteFermeture)
        self.root.bind("<Tab>",self.minimize)
        self.cadreActif=0
        self.perspective=""
        self.perspectives={} #Vue cosmos, systeme solaire, planete, etc
        self.perspectiveCourante=None
        self.perspectiveCouranteTexte="Espace"
        self.canevasCourant=None
        self.selections=[]
        self.boutonS=0
        self.boutonS = 0
        self.etoileSelectionnee=None
        self.planeteSelectionnee=None
        #self.ecranPartie(x,y,x_espace,y_espace)
        self.civMere = StringVar()
        self.civGaz = StringVar()
        self.civGaz.set("")
        self.civMinerai = StringVar()
        self.civEtoiles = StringVar()
        self.civStations = StringVar()
        self.civCollectGaz = StringVar()
        self.civCollectMin = StringVar()
        self.civScout = StringVar()
        self.civCorvette = StringVar()
        self.civFregate = StringVar()
        self.civDestroyer = StringVar()
        self.civColonisateur = StringVar()
        self.civCosmo = StringVar()
        self.civCommand = StringVar()
        self.civUnites = StringVar()
        #self.planeteGaz = StringVar()
        #self.planeteMinerai = StringVar()
        self.creeCadres()
        self.placeCadre(self.cadreConnection)
        self.objetSelect=None
        self.event=None
        self.posCanevasXDansEcran=self.canevasEspace.winfo_rootx()
        self.posCanevasYDansEcran=self.canevasEspace.winfo_rooty()
        self.posSourisXDansEcran=self.canevasEspace.winfo_pointerx()
        self.posSourisYDansEcran=self.canevasEspace.winfo_pointery()
        self.selectionBox=False
        
        #self.civCash = StringVar()
        #self.civEtoiles = StringVar()
        
        #self.civBat = StringVar()
        #self.civVaisseau = StringVar()
        #self.civUnites = StringVar()
        self.batimentACreer=""


    def minimize(self,evt=""):
        self.root.overrideredirect(0)
        self.root.wm_state('iconic')
       

    def captureChat(self,evt):
        txt=self.captureJoueur.get()+": "+self.champChat.get()
        self.parent.captureChat(txt)
        self.champChat.delete(0,END)

    def afficheChat(self,chat):
        self.displayChat.delete(0,END)
        for i in chat:
            self.displayChat.insert(END,i)
        self.displayChat.see(END)

    def creeCadres(self): #Créer les cadres, la premiere fenetre d'ouverture lors du demarage
        self.creeCadreConnection()
        self.creeCadreAttente()
        self.creeCadrePartie()

    def creeCadrePartie(self): #Creer le cadre du jeu
        self.cadrePartie=Frame(self.root)#demarre le frame(le cadre principal)
        self.cadrePartie.rowconfigure(1,weight=1)
        self.cadrePartie.columnconfigure(0,weight=1)
        print("Creer cadres de jeu")
        self.creeCadreMenuPartie() #Cadres de jeu
        self.creeCadreCosmos()
        self.creeCadreEspace()
        self.creeCadrePlanete()
        self.creeCadreSysteme()

        self.perspectives={"Cosmos":self.cadreCosmos,
                           "Espace":self.cadreEspace,
                           "Systeme":self.cadreSysteme,
                           "Planete":self.cadrePlanete
                          }
        self.canevas={"Cosmos":self.canevasCosmos,
                           "Espace":self.canevasEspace,
                           "Systeme":self.canevasSysteme,
                           "Planete":self.canevasPlanete
                           }
        self.creeCadreInfosCiv()
        #self.creeCadreInfoArtefact()
        self.creeCadreNiveauVue()
        self.creeCadreInferieur()

    def creeCadreMenuPartie(self):
        self.cadreMenuPartie=Frame(self.cadrePartie,height=40,bg="grey25")
        self.cadreMenuPartie.grid_propagate(0)#fixer le frame, non modifiable

        trouveL=Frame(self.cadreMenuPartie,height=10,bg="grey25")
        trouveL.grid(row=5,column=10,sticky=N)# N = nord(en haut)
        #trouveB=Buttonjm(self.cadreMenuPartie,text="Trouve planete",command=self.centrerPlanete)#button Trouve planete dans le frame trouveL
        #trouveB.grid(row=10,column=10,sticky=N)
        #self.showB=Buttonjm(self.cadreMenuPartie,text="showPlanete",relief=GROOVE,command=self.showPlanete)
        #self.showB.grid(row=10,column=20,sticky=N)

    def creeCadreCosmos(self):
        self.cadreCosmos=Frame(self.cadrePartie)#Création du cadre cosmos
        self.cadreCosmos.rowconfigure(0,weight=1)#pour configurer une rangee
        self.cadreCosmos.columnconfigure(0,weight=1)#pour configurer une colonne

        self.cadreCosmos.sy=Scrollbar(self.cadreCosmos,orient=VERTICAL) #Ajout de scrollbars
        self.cadreCosmos.sx=Scrollbar(self.cadreCosmos,orient=HORIZONTAL) #Ajout de scrollbars
        self.canevasCosmos=Canvas(self.cadreCosmos,width=self.largeurFenetreJeu,height=self.hauteurFenetreJeu,
                            xscrollcommand=self.cadreCosmos.sx.set,yscrollcommand=self.cadreCosmos.sy.set,bd=0, #set = setter le scrollbar x ou y, bd = distance
                            scrollregion=(0,0,self.largeurFenetreJeu,self.hauteurFenetreJeu),bg="black")
        self.cadreCosmos.sx.config(command=self.canevasCosmos.xview)#vue horizontale si on scroll a droite ou a gauche
        self.cadreCosmos.sy.config(command=self.canevasCosmos.yview)#vue verticale si on scroll vers le haut ou vers le bas
        self.canevasCosmos.grid(row=0,column=0,sticky=N+S+W+E)
        self.cadreCosmos.sy.grid(row=0,column=1,sticky=N+S)
        self.cadreCosmos.sx.grid(row=1,column=0,sticky=W+E)
        self.canevasCosmos.bind("<Control-1>",self.selectObject)
        self.canevasCosmos.bind("<Configure>",self.initCosmos)

    def creeCadreSysteme(self): #Création du cadre systeme
        self.cadreSysteme=Frame(self.cadrePartie)
        self.cadreSysteme.rowconfigure(0,weight=1)
        self.cadreSysteme.columnconfigure(0,weight=1)

        self.cadreSysteme.sy=Scrollbar(self.cadreSysteme,orient=VERTICAL) #Ajout de scrollbars
        self.cadreSysteme.sx=Scrollbar(self.cadreSysteme,orient=HORIZONTAL) #Ajout de scrollbars
        self.canevasSysteme=Canvas(self.cadreSysteme,width=self.largeurFenetreJeu,height=self.hauteurFenetreJeu,
                            xscrollcommand=self.cadreSysteme.sx.set,yscrollcommand=self.cadreSysteme.sy.set,bd=0,
                            bg="black")
        self.cadreSysteme.sx.config(command=self.canevasSysteme.xview)
        self.cadreSysteme.sy.config(command=self.canevasSysteme.yview)

        self.canevasSysteme.grid(row=0,column=0,sticky=N+S+W+E)
        self.cadreSysteme.sy.grid(row=0,column=1,sticky=N+S)
        self.cadreSysteme.sx.grid(row=1,column=0,sticky=W+E)
        self.canevasSysteme.bind("<Control-1>",self.selectObject)
        self.canevasSysteme.bind("<Configure>",self.initSysteme)
        self.canevasSysteme.bind("<Button-1>",self.selectObject)

    def creeCadrePlanete(self): #Création du cadre planete
        self.cadrePlanete=Frame(self.cadrePartie)
        self.cadrePlanete.rowconfigure(0,weight=1)
        self.cadrePlanete.columnconfigure(0,weight=1)

        self.cadrePlanete.sy=Scrollbar(self.cadrePlanete,orient=VERTICAL) #Ajout de scrollbars
        self.cadrePlanete.sx=Scrollbar(self.cadrePlanete,orient=HORIZONTAL) #Ajout de scrollbars
        #xscrollcommand=sx.set veut dire on connecte le scrollbar au canva
        self.canevasPlanete=Canvas(self.cadrePlanete,width=self.largeurFenetreJeu,height=self.hauteurFenetreJeu,
                            xscrollcommand=self.cadrePlanete.sx.set,yscrollcommand=self.cadrePlanete.sy.set,bd=0,
                            scrollregion=(0,0,self.x_planete,self.y_planete),bg="wheat4")
        self.cadrePlanete.sx.config(command=self.canevasPlanete.xview)
        self.cadrePlanete.sy.config(command=self.canevasPlanete.yview)
        self.canevasPlanete.grid(row=0,column=0,sticky=N+S+W+E)
        self.cadrePlanete.sy.grid(row=0,column=1,sticky=N+S)
        self.cadrePlanete.sx.grid(row=1,column=0,sticky=W+E)
        self.canevasPlanete.bind("<Control-1>",self.selectObject)
        self.canevasPlanete.bind("<Configure>",self.initPlanete)
        self.canevasPlanete.bind("<Button-1>",self.selectObject)

    def creeCadreEspace(self):
        self.cadreEspace=Frame(self.cadrePartie)
        self.cadreEspace.rowconfigure(0,weight=1)
        self.cadreEspace.columnconfigure(0,weight=1)

        self.cadreEspace.sy=Scrollbar(self.cadreEspace,orient=VERTICAL)
        self.cadreEspace.sx=Scrollbar(self.cadreEspace,orient=HORIZONTAL)
        self.canevasEspace=Canvas(self.cadreEspace,width=self.largeurFenetreJeu,height=self.hauteurFenetreJeu,
                            xscrollcommand=self.cadreEspace.sx.set,yscrollcommand=self.cadreEspace.sy.set,bd=0,
                            scrollregion=(0,0,self.x_espace,self.y_espace),bg="black")
        self.cadreEspace.sx.config(command=self.canevasEspace.xview)
        self.cadreEspace.sy.config(command=self.canevasEspace.yview)

        self.canevasEspace.bind("<Control-1>",self.clicAddObject)
        self.canevasEspace.bind("<Button-1>",self.clicSelectObject)
        self.canevasEspace.bind("<Button-3>",self.changeCible)
        self.canevasEspace.bind("<B1-Motion>",self.drawSelectionBox)
        self.canevasEspace.bind("<ButtonRelease-1>",self.releaseSelectionBox)
        self.canevasEspace.bind("<Configure>",self.initEspace)
        self.canevasEspace.grid(row=0,column=0,sticky=N+S+W+E)
        self.cadreEspace.sy.grid(row=0,column=1,sticky=N+S)
        self.cadreEspace.sx.grid(row=1,column=0,sticky=W+E)

    def selectObject(self,evt):#Selectionner un objet
        x=evt.x
        y=evt.y
        x=self.canevasPlanete.canvasx(x)
        y=self.canevasPlanete.canvasy(y)
        t = self.canevasCourant.gettags(CURRENT)
        print("Bite me 12345 ", t)
        self.selectX = self.canevasEspace.canvasx(evt.x)
        self.selectY = self.canevasEspace.canvasy(evt.y)
        self.selection = [] #Add object -- to be continued
        self.event = evt
        self.canevasEspace.delete("EtoileSelectionnee")
       
        for i in self.partie.civs.keys():
            for j in self.parent.modele.civs[i].entites.keys():
                if j == "Batiments":
                    for z in self.parent.modele.civs[i].entites[j]:
                         if self.planeteSelectionnee in self.parent.modele.civs[i].planetesColonisees:
                             if self.planeteSelectionnee.noPlanete == z.idPlanete:
                                 if self.perspectiveCouranteTexte=="Planete":
                                     if self.batimentACreer!="":
                                         print(" ?? ",self.batimentACreer,i,z,z.id)
                                         if self.batimentACreer == "CollecteurGaz":
                                             self.canevasPlanete.create_rectangle(x-25,y-25,x+25,y+25,fill="green",tags=("artefact",i,z.id,"Batiments"))
                                             self.canevasPlanete.create_rectangle(x-10,y-10,x+10,y+10,fill="white",tags=("artefact",i,z.id,"Batiments"))
                                             self.root.config(cursor="arrow")
                                             self.canevasPlanete.unbind("<Motion>")
                                             self.canevasPlanete.delete("collecteurGazAPlacer")
                                             self.batimentACreer=""
                                         if self.batimentACreer == "CollecteurMinerais":
                                             self.canevasPlanete.create_rectangle(x-25,y-25,x+25,y+25,fill="DarkGray",tags=("artefact",i,z.id,"Batiments"))
                                             self.canevasPlanete.create_rectangle(x-10,y-10,x+10,y+10,fill="white",tags=("artefact",i,z.id,"Batiments"))
                                             self.root.config(cursor="arrow")
                                             self.canevasPlanete.unbind("<Motion>")
                                             self.canevasPlanete.delete("collecteurMineraisAPlacer")
                                             self.batimentACreer=""
                                         if self.batimentACreer == "Cosmolab":
                                             self.canevasPlanete.create_rectangle(x-25,y-25,x+25,y+25,fill="navy",tags=("artefact",i,z.id,"Batiments"))
                                             self.canevasPlanete.create_rectangle(x-10,y-10,x+10,y+10,fill="orange",tags=("artefact",i,z.id,"Batiments"))
                                             self.root.config(cursor="arrow")
                                             self.canevasPlanete.unbind("<Motion>")
                                             self.canevasPlanete.delete("cosmoLabAPlacer")
                                             self.batimentACreer=""

        if t:
            if "Etoiles" not in t and self.perspectiveCouranteTexte=="Espace":
                self.etoileSelectionnee=None
            if "Planete" not in t and self.perspectiveCouranteTexte=="Systeme":
                self.planeteSelectionnee=None
                
            if "Vaisseaux" in t: #si on clique sur un vaisseau
                self.boutonS = 1
                if t[1]==self.parent.nom:
                    for i in self.modele.civs[t[1]].entites["Vaisseaux"]:
                        if i.id==int(t[2]):
                            print("Vaisseau : ", i)
                            self.selection.append(i)
                            self.objetSelect=i
                            self.boutonS=2
                            if i.type=="Colonisateur":
                                self.colonisation()
                                self.boutonS = 6
            elif "Station" in t:
                self.boutonS = 1
                if t[1]==self.parent.nom:
                    for i in self.modele.civs[t[1]].entites["Station"]:
                        if i.id==int(t[2]):
                            print("Station : ", i)
                            self.selection.append(i)
                            self.objetSelect=i
                            self.boutonS = 3
                            
            elif "Batiments" in t:
                 print("**On est dans selectionObjet Batiments **")
                 self.boutonS = 1
                 print("NOM : ",t,self.parent.nom)
                 if t[1]==self.parent.nom:
                    print("CC", self.modele.civs[t[1]].entites["Batiments"])
                    for i in self.modele.civs[t[1]].entites["Batiments"]:
                        print("grrr",i,i.id)
                        if i.id==int(t[2]):
                            print("Batiments : ", i)
                            if i.type=="CommandCenter":
                                self.boutonS = 7
                        
                            if i.type=="Cosmolab":
                                self.boutonS = 8
                                
                            if i.type=="CollecteurGaz":
                                self.boutonS = 9
                                
                            if i.type=="CollecteurMinerais":
                                self.boutonS = 10

                        self.selection.append(i)
                        self.objetSelect=i
                                
            elif "Etoiles" in t:
                self.etoileSelectionnee=self.parent.modele.etoiles[int(t[1])]
                print( "Etoile séléctionnée : ", self.parent.modele.etoiles[int(t[1])])
                self.objetSelect=self.etoileSelectionnee
                #self.canevasEspace.delete("EtoileSelectionnee")
                self.dessineEtoileSelectionnee()
                self.bsysteme.config(state=NORMAL)
                self.boutonS=4
            elif "Planete" in t:
                self.planeteSelectionnee=self.etoileSelectionnee.planetes[int(t[1])]
                self.objetSelect=self.planeteSelectionnee
                self.bplanete.config(state=NORMAL)
                #if int(t[1])==self.parent.modele.civs[self.parent.nom].planeteMere.noPlanete:
                for i in self.modele.civs[self.parent.nom].etoilesVisites:
                    if self.etoileSelectionnee == i:
                        for z in range(len(self.modele.civs[self.parent.nom].etoilesVisites)):
                            for j in self.modele.civs[self.parent.nom].etoilesVisites[z].planetes:
                                if j.noPlanete==int(t[1]):
                                    print("Planete ",j)
                                    self.objetSelect=j
                                    self.selection.append(j)
                self.canevasSysteme.delete("planeteSelectionnee")
                self.planeteSelectionnee=self.etoileSelectionnee.planetes[int(t[1])]
                self.bplanete.config(state=NORMAL)
                self.boutonS=5
                self.dessinePlaneteSelectionne()
                
        #si on clique sur rien        
        else:
            #si on est dans systeme et qu'on clique ds le vide
            if(self.perspectiveCouranteTexte=="Systeme"):
                self.canevasSysteme.delete("planeteSelectionnee")
                self.planeteSelectionnee=None
                self.bplanete.config(state=DISABLED)

            self.boutonS=1
            #si on est dans espace et qu'on clique ds le vide
            if self.perspectiveCouranteTexte=="Espace":
                self.etoileSelectionnee=None
        
        
        if self.etoileSelectionnee==None:
            self.bsysteme.config(state=DISABLED)
        
        self.afficherBoutonsSelection()
        #self.artefacttype1.config(text=t)
        self.parent.selectObject(self.selection)
        
    def clicSelectObject(self,evt):
        self.t=self.canevasCourant.gettags(CURRENT)
        self.selectObject(evt)
        self.afficherCadreInfoEntite()
        
    def clicAddObject(self,evt):
        self.t=self.canevasCourant.gettags(CURRENT)
        self.addObject(evt)
        self.afficherCadreInfoEntite()

    def addObject(self,evt=""): #Ajoute un objet a la selection
        t=self.t
        print("byte me 2 ", t)
        if t:
            if "Vaisseaux" in t: #s'il y a des vaisseaux dans le canvas actuel
                print("Vaisseaux")
                if t[1]==self.parent.nom:
                    for i in self.modele.civs[t[1]].entites["Vaisseaux"]:
                        if i.id==int(t[2]):
                            if i in self.selection:
                                self.selection.remove(i)
                            else:
                                self.selection.append(i)
                            self.afficheSelection()
                        else:
                            print("PAS BON VAISSEAU")
                else:
                    print("PAS MOI")
            elif "Station" in t:
                if t[1]==self.parent.nom:
                    for i in self.modele.civs[t[1]].entites["Station"]:
                        if i.id==int(t[2]):
                            if i in self.selection:
                                self.selection.remove(i)
                            else:
                                self.selection.append(i)
                            self.afficheSelection()
            elif "Etoiles" in t:
                print("Selection d'une etoile")

        elif self.selection:
            #input(self.selection)
            #self.changeCible(self.selection,evt)
            self.selection=[]
            self.canevasEspace.delete("selection")
        else:
            self.selection=[]
        #self.artefacttype1.config(text=t)
        self.parent.selectObject(self.selection)



############# Creation des Vaisseaux ################

    def ajouterScout(self):
        print("Scout : ",self.objetSelect)
        self.parent.creerVaisseau(self.objetSelect,"Scout")
        print("Scout passe dans le controleur")
        self.afficheSelection()

    def ajouterCorvette(self):
        print("Corvette: ",self.objetSelect)
        self.parent.creerVaisseau(self.objetSelect,"Corvette")
        print("Corvette passe dans le controleur")
        self.afficheSelection()

    def ajouterColonisateur(self):
        print("COLONISATEUR: ",self.objetSelect)
        self.parent.creerVaisseau(self.objetSelect,"Colonisateur")
        print("Colonisateur passe dans le controleur")
        self.afficheSelection()#
        
    def ajouterFregate(self):
        print("Fregate: ",self.objetSelect)
        self.parent.creerVaisseau(self.objetSelect,"Fregate")
        print("Fregate passe dans le controleur")
        self.afficheSelection()
        
    def ajouterDestroyer(self):
        print("DESTROYER: ",self.objetSelect)
        self.parent.creerVaisseau(self.objetSelect,"Destroyer")
        print("Destroyer passe dans le controleur")
        self.afficheSelection()

############# Creation des Vaisseaux ################

############# Creation des Batiments ################

    def ajouterStation(self):
        print("STATION : ",self.objetSelect)
        self.parent.creerStation(self.objetSelect)
        print("Batiment passe dans le controleur")
        self.afficheSelection()

    def ajouterCollecteurGaz(self):
        print("COLLECTEUR A GAZ : ",self.objetSelect)
        self.batimentACreer="CollecteurGaz"
        self.canevasPlanete.bind("<Motion>", self.dessineCollecteurGaz)
        self.parent.creerBatiment(self.objetSelect,"CollecteurGaz", self.etoileSelectionnee)
        self.root.config(cursor="")
        print("Collecteur a gaz passe dans le controleur")
        self.afficheSelection()

    def ajouterCollecteurMinerais(self):
        print("COLLECTEUR de MINERAIS : ",self.objetSelect)
        self.batimentACreer="CollecteurMinerais"
        self.canevasPlanete.bind("<Motion>", self.dessineCollecteurMinerais)
        self.parent.creerBatiment(self.objetSelect,"CollecteurMinerais", self.etoileSelectionnee)
        self.root.config(cursor="")
        print("Collecteur de MINERAIS passe dans le controleur")
        self.afficheSelection()

    def ajouterCommandCenter(self):
        print("COMMAND CENTER : ",self.objetSelect)
        self.batimentACreer="commandCenter"
        #self.canevasPlanete.bind("<Motion>",self.dessineCommandCenterAPlacer)
        self.root.config(cursor="")
        self.parent.creerBatiment(self.objetSelect,"CommandCenter",self.etoileSelectionnee)
        print("Command Center passe dans le controleur")
        self.afficheSelection()

    def ajouterCosmolab(self):
        print("COSMO LAB : ",self.objetSelect)
        self.batimentACreer="Cosmolab"
        self.canevasPlanete.bind("<Motion>",self.dessineCosmoLab)
        self.root.config(cursor="")
        print("cosmolab passe dans le controleur")
        self.afficheSelection()#
        self.parent.creerBatiment(self.objetSelect,"Cosmolab",self.etoileSelectionnee)

    def dessineCommandCenterAPlacer(self,evt=""):
        self.canevasPlanete.delete("commandCenterAPlacer")
        x=evt.x
        y=evt.y
        print("xy1=",x,y)
        x=self.canevasPlanete.canvasx(x)
        y=self.canevasPlanete.canvasy(y)
        print("xy2=",x,y)

        self.canevasPlanete.create_rectangle(x-35,y-35,x+35,y+35,fill="red",tags="commandCenterAPlacer")
        
    def dessineCollecteurGaz(self, evt=""):
        self.canevasPlanete.delete("collecteurGazAPlacer") 
        x=evt.x
        y=evt.y
        print("xy1=",x,y)
        x=self.canevasPlanete.canvasx(x)
        y=self.canevasPlanete.canvasy(y)
        print("xy2=",x,y)
        
        self.canevasPlanete.create_rectangle(x-35,y-35,x+35,y+35,fill="blue",tags="collecteurGazAPlacer")
        
    def dessineCollecteurMinerais(self, evt=""):
        self.canevasPlanete.delete("collecteurMineraisAPlacer") 
        x=evt.x
        y=evt.y
        print("xy1=",x,y)
        x=self.canevasPlanete.canvasx(x)
        y=self.canevasPlanete.canvasy(y)
        print("xy2=",x,y)
        
        self.canevasPlanete.create_rectangle(x-35,y-35,x+35,y+35,fill="blue",tags="collecteurMineraisAPlacer")

    def dessineCosmoLab(self, evt=""):
        self.canevasPlanete.delete("cosmoLabAPlacer") 
        x=evt.x
        y=evt.y
        print("xy1=",x,y)
        x=self.canevasPlanete.canvasx(x)
        y=self.canevasPlanete.canvasy(y)
        print("xy2=",x,y)
        
        self.canevasPlanete.create_rectangle(x-35,y-35,x+35,y+35,fill="orange",tags="cosmoLabAPlacer")
        
        
############# Creation des Batiments ################



    #def creeCadreInfoArtefact(self):
        #f=Font(size=7)#texte de grandeur 7
        #self.cadreInfoArtefact=Frame(self.cadrePartie,width=200, height=200, bg="grey25")
        #self.cadreInfoArtefact.grid_propagate(0)

        #artefacttypel=Labeljm(self.cadreInfoArtefact,text="Objet 1")
        #artefacttypel.grid(column=0,row=0,sticky=W)
        #self.artefacttype1=Labeljm(self.cadreInfoArtefact,text="--",font=f,bg="darkgreen",fg="orange")
        #self.artefacttype1.grid(column=0,row=1,sticky=W)
        #artefacttypel=Labeljm(self.cadreInfoArtefact,text="Objet 2")
        #artefacttypel.grid(column=0,row=2,sticky=W)
        #self.artefacttype2=Labeljm(self.cadreInfoArtefact,text="--")
        #self.artefacttype2.grid(column=0,row=3,sticky=W)

        #civi= Labeljm(self.cadreInfoArtefact, text="Civilisation : ")
        #civi.grid(column=0,row=3,sticky=W)
        #self.civi=Labeljm(self.cadreInfoArtefact,text=self.parent.modele,font=f)
        #self.civi.grid(column=0,row=1,sticky=W)

        #vaiss= Labeljm(self.cadreInfoArtefact, text="Vaisseaux : ")
        #vaiss.grid(column=0,row=4,sticky=W)
        #self.vaiss=Labeljm(self.cadreInfoArtefact,text=self.parent.modele,font=f)
        #self.vaiss.grid(column=0,row=1,sticky=W)

        #self.cadreInfoArtefact2=Frame(self.cadreInfoArtefact,width=200,bg="grey25")
        #self.cadreInfoArtefact2.grid_propagate(1)

        #self.cadreInfoArtefact2.grid(column=0,row=6,sticky=W)
        #exploSystB=Buttonjm(self.cadreInfoArtefact2,text="Explore SystÄ?me",command=self.exploreSysteme)
        #exploSystB.grid(column=0,row=0,sticky=W+E)
        #colonisePlaneteB=Buttonjm(self.cadreInfoArtefact2,text="Colonise Planete",command=self.colonisePlanete)
        #colonisePlaneteB.grid(column=0,row=1,sticky=W+E)
        #trouveB=Buttonjm(self.cadreInfoArtefact2,text="Trouve Etoile Mere",command=self.centrerPlanete) #le 1er button en bas
        #trouveB.grid(row=5,column=0,sticky=E, padx=10, pady=10)


    def dataCadreInfosCiv(self):
        #self.civMere.set(str(self.modele.civs[self.moi].planeteMere))
        self.civGaz.set(str(self.modele.civs[self.moi].gaz))
        self.civMinerai.set(str(self.modele.civs[self.moi].minerai))
        #self.civEtoiles.set(str(self.modele.civs[self.moi].nbPlanetes))
        self.civScout.set(str(self.modele.civs[self.moi].nbScout))
        self.civCorvette.set(str(self.modele.civs[self.moi].nbCorvette))
        self.civFregate.set(str(self.modele.civs[self.moi].nbFregate))
        self.civDestroyer.set(str(self.modele.civs[self.moi].nbDestroyer))
        self.civColonisateur.set(str(self.modele.civs[self.moi].nbColonisateur))
        self.civCollectGaz.set(str(self.modele.civs[self.moi].nbCollectGaz))
        self.civCollectMin.set(str(self.modele.civs[self.moi].nbCollectMin))
        self.civCommand.set(str(self.modele.civs[self.moi].nbCommandCenter))
        self.civCosmo.set(str(self.modele.civs[self.moi].nbCosmoLab))
        
        #self.textGaz.set(str(self.modele.Planete[self.moi].gazPlanete))
        #self.textMinerai.set(str(self.modele.Planete[self.moi].minPlanete))
        #self.civStations.set(self.modele.)
        
        #self.civUnites.set
        
    
    def creeCadreInfosCiv(self):
        self.cadreInfosCiv=Frame(self.cadrePartie,width=200,bg="grey25")
        self.cadreInfosCiv.grid_propagate(0)
        
        #labelPlaneteMere=Labeljm(self.cadreInfosCiv, text="Planete Mere: ", font=("Helvetica", 8), bd=10)
        #labelPlaneteMere.grid(row=0, column=0, sticky=W)
        #self.labelPlaneteMere=Labeljm(self.cadreInfosCiv, textvariable=self.civMere)
        #self.labelPlaneteMere.grid(row=0, column=1, sticky=W)
        
        labelGaz=Labeljm(self.cadreInfosCiv, text="Gaz: ", font=("Helvetica", 8), bd=10)
        labelGaz.grid(row=1, column=0, sticky=W)
        self.labelGaz=Labeljm(self.cadreInfosCiv, textvariable=self.civGaz)
        self.labelGaz.grid(row=1, column=1, sticky=W)
        
        labelMinerai=Labeljm(self.cadreInfosCiv, text="Minerai: ", font=("Helvetica", 8), bd=10)
        labelMinerai.grid(row=2, column=0, sticky=W)
        self.labelMinerai=Labeljm(self.cadreInfosCiv, textvariable=self.civMinerai)
        self.labelMinerai.grid(row=2, column=1, sticky=W)
        
        #labelEtoiles=Labeljm(self.cadreInfosCiv, text="Etoiles visitees: ", font=("Helvetica", 8), bd=10)
        #labelEtoiles.grid(row=3, column=0, sticky=W)
        #self.labelEtoiles=Labeljm(self.cadreInfosCiv, textvariable=self.civEtoiles)
        #self.labelEtoiles.grid(row=3, column=1, sticky=W)
        
        #labelStations=Labeljm(self.cadreInfosCiv, text="Stations: ", font=("Helvetica", 8), bd=10)
        #labelStations.grid(row=4, column=0, sticky=W)
        
        labelScout=Labeljm(self.cadreInfosCiv, text="Scouts: ", font=("Helvetica", 8), bd=10)
        labelScout.grid(row=5, column=0, sticky=W)
        self.labelScout=Labeljm(self.cadreInfosCiv, textvariable=self.civScout)
        self.labelScout.grid(row=5, column=1, sticky=W)
        
        labelCorvette=Labeljm(self.cadreInfosCiv, text="Corvettes: ", font=("Helvetica", 8), bd=10)
        labelCorvette.grid(row=6, column=0, sticky=W)
        self.labelCorvette=Labeljm(self.cadreInfosCiv, textvariable=self.civCorvette)
        self.labelCorvette.grid(row=6, column=1, sticky=W)
        
        labelFregate=Labeljm(self.cadreInfosCiv, text="Fregates: ", font=("Helvetica", 8), bd=10)
        labelFregate.grid(row=7, column=0, sticky=W)
        self.labelFregate=Labeljm(self.cadreInfosCiv, textvariable=self.civFregate)
        self.labelFregate.grid(row=7, column=1, sticky=W)
        
        labelDestroyer=Labeljm(self.cadreInfosCiv, text="Destroyers: ", font=("Helvetica", 8), bd=10)
        labelDestroyer.grid(row=8, column=0, sticky=W)
        self.labelDestroyer=Labeljm(self.cadreInfosCiv, textvariable=self.civDestroyer)
        self.labelDestroyer.grid(row=8, column=1, sticky=W)
        
        labelColonisateur=Labeljm(self.cadreInfosCiv, text="MotherShip: ", font=("Helvetica", 8), bd=10)
        labelColonisateur.grid(row=9, column=0, sticky=W)
        self.labelColonisateur=Labeljm(self.cadreInfosCiv, textvariable=self.civColonisateur)
        self.labelColonisateur.grid(row=9, column=1, sticky=W)
        
        labelPetroliere=Labeljm(self.cadreInfosCiv, text="Collecteurs Gaz: ", font=("Helvetica", 8), bd=10)
        labelPetroliere.grid(row=10, column=0, sticky=W)
        self.labelPetroliere=Labeljm(self.cadreInfosCiv, textvariable=self.civCollectGaz)
        self.labelPetroliere.grid(row=10, column=1, sticky=W)
        
        labelMine=Labeljm(self.cadreInfosCiv, text="Collecteurs Minerai: ", font=("Helvetica", 8), bd=10)
        labelMine.grid(row=11, column=0, sticky=W)
        self.labelMine=Labeljm(self.cadreInfosCiv, textvariable=self.civCollectMin)
        self.labelMine.grid(row=11, column=1, sticky=W)
        
        labelCommand=Labeljm(self.cadreInfosCiv, text="Centre de commandes: ", font=("Helvetica", 8), bd=10)
        labelCommand.grid(row=12, column=0, sticky=W)
        self.labelCommand=Labeljm(self.cadreInfosCiv, textvariable=self.civCommand)
        self.labelCommand.grid(row=12, column=1, sticky=W)
        
        labelCosmoLab=Labeljm(self.cadreInfosCiv, text="CosmoLabs: ", font=("Helvetica", 8), bd=10)
        labelCosmoLab.grid(row=13, column=0, sticky=W)
        self.labelCosmoLab=Labeljm(self.cadreInfosCiv, textvariable=self.civCosmo)
        self.labelCosmoLab.grid(row=13, column=1, sticky=W)
        
        #labelUnites=Labeljm(self.cadreInfosCiv, text="Unites terrestres: ", font=("Helvetica", 8), bd=10)
        #labelUnites.grid(row=11, column=0, sticky=W)
    #def creeCadreCommandeCiv(self):
        #self.cadreCommandeCiv=Frame(self.cadrePartie,height=100,bg="grey25")
        #self.cadreCommandeCiv.grid_propagate(0)

    def creeCadreNiveauVue(self):
        self.cadreNiveauVue=Frame(self.cadrePartie,width=200,height=100,bg="grey25")
        self.cadreNiveauVue.grid_propagate(0) #fixer le frame, non modifiable
        self.bcosmos=Buttonjm(self.cadreNiveauVue,text="Cosmos",command=self.placeCosmos)
        self.bcosmos.grid(column=0,row=0,sticky=E+W, padx=2, pady=2)
        self.bespace=Buttonjm(self.cadreNiveauVue,text="Espace",state=DISABLED,command=self.placeEspace)
        self.bespace.grid(column=1,row=0,sticky=E+W, padx=2, pady=2)
        self.bsysteme=Buttonjm(self.cadreNiveauVue,text="Systeme",state=DISABLED,command=self.placeSysteme)
        self.bsysteme.grid(column=0,row=1,sticky=E+W, padx=2, pady=2)
        self.bplanete=Buttonjm(self.cadreNiveauVue,text="Planete",state=DISABLED,command=self.placePlanete)
        self.bplanete.grid(column=1,row=1,sticky=E+W, padx=2, pady=2)
        self.trouveB=Buttonjm(self.cadreNiveauVue,text="Trouve Etoile Mere",command=self.centrerPlanete) #le 1er button en bas
        self.trouveB.grid(row=5,column=0,sticky=E, padx=10, pady=10, columnspan=2)

    def placeCadreNiveau(self,p):
        self.perspectiveDepart=self.perspectiveCouranteTexte[:]
        self.perspectiveCouranteTexte=p
        self.griserBoutonPerspective(self.perspectiveDepart,self.perspectiveCouranteTexte)
        
        
       
        if self.perspectiveDepart=="Planete":
            self.planeteSelectionnee==None
            self.bplanete.config(state=DISABLED)
            if self.perspectiveCouranteTexte!="Systeme":
                self.etoileSelectionnee==None
                self.bsysteme.config(state=DISABLED)
        
        if self.perspectiveDepart=="Systeme" and self.perspectiveCouranteTexte!="Planete":
            self.etoileSelectionnee==None
            self.planeteSelectionnee==None
            self.bplanete.config(state=DISABLED)
            self.bsysteme.config(state=DISABLED)
            

        #si on clique sur la perspective qu'on est deja, ca pass
        if self.perspectiveDepart==self.perspectiveCouranteTexte:
            pass
        #si on se deplace vers Systeme, on doit refaire un initsyteme
        #car chaque systeme est different
        elif self.perspectiveCouranteTexte == "Systeme" :
                self.initSysteme()
                self.placePerspective(self.perspectiveCouranteTexte)
        else:
            self.placePerspective(self.perspectiveCouranteTexte)
            

    def intercepteFermeture(self,evt=""): #quand le joueur quitte le jeu
        print("Je me ferme")
        self.parent.jeQuitte()
        self.root.destroy()

    def afficheAttente(self):
        self.placeCadre(self.cadreAttente)

    def placeCadre(self,c):  #place un cadre actif et set les autres inactifs
        if self.cadreActif:
            self.cadreActif.pack_forget()
        self.cadreActif=c
        self.cadreActif.pack(expand=1,fill=BOTH)

    def creeCadreConnection(self): #creer une fenetre de connexion
        self.cadreConnection=Frame(self.root)
        cadreMenu=Frame(self.cadreConnection)#creer le cadre menu

        Nom=Labeljm(cadreMenu,text="Entrez votre nom")
        self.captureJoueur=Entry(cadreMenu, justify=CENTER)
        self.captureJoueur.insert(0,"joueur "+str(random.randrange(100)))  #champ texte: nom joueur
        Nom.grid(column=0,row=0, sticky=N+S+E+W, padx=10, pady=10)
        self.captureJoueur.grid(column=2,row=0, padx=10, pady=10, sticky=N+S+E+W)

        """lcree=Labeljm(cadreMenu,text="Pour créer un serveur ŕ l'adresse inscrite")
        lconnect=Labeljm(cadreMenu,text="Pour vous connecter ŕ un serveur")
        lcree.grid(column=0,row=1)
        lconnect.grid(column=2,row=1)"""
        
        lip=Labeljm(cadreMenu,text=self.parent.monip) #monip = mon adresse ip
        self.autreip=Entry(cadreMenu, justify=CENTER)
        self.autreip.insert(0,self.parent.monip)
        lip.grid(column=0,row=2, padx=10, pady=10, sticky=N+S+E+W)
        self.autreip.grid(column=2,row=2, padx=10, pady=10, sticky=N+S+E+W)

        creerB=Buttonjm(cadreMenu,text="Creer un serveur",command=self.creerServeur)
        connecterB=Buttonjm(cadreMenu,text="Connecter a un serveur",command=self.connecterServeur)
        creerB.grid(column=0,row=4, padx=10, pady=10, sticky=N+S+E+W)
        connecterB.grid(column=2,row=4, padx=10, pady=10, sticky=N+S+E+W)

        self.galax=PhotoImage(file="ecranDebut.gif")
        galaxl=Labeljm(self.cadreConnection,image=self.galax)
        galaxl.pack()
        cadreMenu.pack()

    def creeCadreAttente(self):  #2e fenetre lors de la connexion
        self.cadreAttente=Frame(self.root)
        cadreMenu=Frame(self.cadreAttente)
        self.listeJoueurs=Listbox(cadreMenu)
        self.demarreB=Buttonjm(cadreMenu,text="Demarre partie",state=DISABLED,command=self.parent.demarrePartie)
        self.demarreB.grid(column=0,row=1, padx=10, pady=10)
        self.ajouterBot=Buttonjm(cadreMenu,text="Ajouter un BOT", command=self.parent.creerBot)
        self.ajouterBot.grid(column=3, row = 1, padx =10,pady=10)
        self.listeJoueurs.grid(column=0,row=0, padx=10, pady=10)
        cadreMenu.pack(side=LEFT)
        self.galax2=PhotoImage(file="ecranDebut.gif")
        galax=Labeljm(self.cadreAttente,image=self.galax2)
        galax.pack(side=RIGHT)

    def afficheListeJoueurs(self,liste):
        self.listeJoueurs.delete(0,END)  # ? END
        for i in liste:
            self.listeJoueurs.insert(END,i)

    def exploreSysteme(self):
        pass
    def colonisePlanete(self):
        pass

    def initPartie(self,modele):
        self.partie=modele
        self.moi=modele.parent.nom

        self.cadreMenuPartie.grid(column=0,row=0,sticky=W+E)
        self.cadreInferieur.grid(column=0,row=2,sticky=W+E)
        
        #self.cadreInfoArtefact.grid(column=1,row=0,sticky=W+E+S+N)
        self.cadreInfosCiv.grid(column=1,row=1,sticky=W+E+N+S)
        self.cadreNiveauVue.grid(column=1,row=2,sticky=W+E+S+N)
        self.perspectiveCourante=self.perspectives["Espace"]  #changer la vue courante
        self.placePerspective("Espace")
        self.placeCadre(self.cadrePartie) # Et place le cadre au frame
        self.root.bind("<Motion>",self.xy_motionWindow)
        
    def placePerspective(self,p):  # change la vue courrante pour la vue selectionner (cosmos, espace, etc) ET L'AFFICHE
        self.deleteBoutons()
        print("perspective courante est",self.perspectiveCouranteTexte)
        self.canevasEspace.delete("etoileSelectionnee")
        self.perspectiveCourante.grid_remove()
        self.perspectiveCourante=self.perspectives[p]
        self.perspectiveCourante.grid(column=0,row=1,sticky=W+E+S+N)
        self.canevasCourant=self.canevas[p]
        if p=="Planete":
            self.centrerVuePlanete()
            self.creeCommandCenter.grid(row=2, column=2, sticky=W+E, pady=3, columnspan=1)
            self.creeCollecteurMinerais.config(state=NORMAL)
            self.creeCollecteurGaz.config(state=NORMAL)
            self.creeSpatiolab.config(state=DISABLED)
        else:
            self.creeCollecteurMinerais.config(state=DISABLED)
            self.creeCollecteurGaz.config(state=DISABLED)
        if p=="Espace":
            self.creeSpatiolab.config(state=NORMAL)
        



    def initCosmos(self,evt=""):            # initialise les etoiles
        self.canevasCosmos.delete(ALL)
        divx=self.x_cosmos/self.canevasCosmos.winfo_width() # divise la longueur de l'espace avec la longueur du canevasCosmos
        divy=self.y_cosmos/self.canevasCosmos.winfo_height()# divise la hauteur de l'espace avec la hauteur du canevasCosmos
        taille=1
        for i in self.partie.etoiles: # Affichage des etoiles
            x=i.x/divx
            y=i.y/divy
            self.canevasCosmos.create_oval(x-taille,y-taille,x+taille,y+taille, fill="yellow",tags=("fond","Etoile",i.id)) # Creer etoile
        self.afficheCosmos() #Affiche les etoiles<

    def afficheCosmos(self): #Affiche les etoiles
        self.canevasCosmos.delete("miseajour")
        divx=self.x_cosmos/self.canevasCosmos.winfo_width()
        divy=self.y_cosmos/self.canevasCosmos.winfo_height()
        joueur=self.partie.civs[self.moi]
        etoileMere=joueur.etoileMere
        x=etoileMere.x/divx
        y=etoileMere.y/divy
        #Dessine l'etoile Mere
        n=1
        self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, fill=joueur.couleur,tags=("miseajour","Etoile",etoileMere.id,joueur.nom,"Etoilemere"))
        #Dessine un arc autour de l'etoile Mere
        n=3
        self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, outline=joueur.couleur,tags=("miseajour","Etoile",etoileMere.id,joueur.nom,"Etoilemere"))

    def initSysteme(self,evt=""):    # initialise la vue du systeme
        self.canevasSysteme.delete(ALL)
        
        divx=self.x_systeme/self.canevasSysteme.winfo_width()
        divy=self.y_systeme/self.canevasSysteme.winfo_height()
        print("nb planetes de l'etoile",len(self.etoileSelectionnee.planetes))
        for i in self.etoileSelectionnee.planetes:
            x=i.x/divx
            y=i.y/divy
            self.canevasSysteme.create_oval(x-i.taille,y-i.taille,x+i.taille,y+i.taille, fill=i.couleur,tags=("Planete",i.noPlanete,self.etoileSelectionnee.id))
            if(i.habitee==1):
                self.canevasSysteme.create_rectangle(x-5,y-5,x+5,y+5,fill="white",tags=("drapeau"))
        self.afficheSysteme() #affiche les elements qui ne sont pas les elements
        #de base du systeme



    def afficheSysteme(self):
        self.canevasSysteme.delete("miseajour")
        divx=self.x_systeme/self.canevasCosmos.winfo_width()
        divy=self.y_systeme/self.canevasCosmos.winfo_height()
        n=1
        j=self.partie.civs[self.moi]
        i=j.etoileMere
        x=i.x/divx
        y=i.y/divy
        self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, fill=j.couleur,tags=("miseajour","Etoile",i.id,j.nom,"Etoilemere"))
        n=3
        self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, outline=j.couleur,tags=("miseajour","Etoile",i.id,j.nom,"Etoilemere"))

    def initPlanete(self,evt=""):          #Initialise la vue de la planete
        self.canevasPlanete.delete(ALL)
        divx=self.x_planete/self.canevasPlanete.winfo_width()
        divy=self.y_planete/self.canevasPlanete.winfo_height()
        self.affichePlanete()

    def affichePlanete(self):  # Affiche les planetes
        divx=self.x_espace/self.canevasPlanete.winfo_width()
        divy=self.y_espace/self.canevasPlanete.winfo_height()
        n=1
        j=self.partie.civs[self.moi]
        i=j.etoileMere
        x=i.x/divx
        y=i.y/divy
        self.canevasPlanete.create_oval(x-n,y-n,x+n,y+n, fill=j.couleur,tags=("miseajour","Etoile",i.id,j.nom,"Etoilemere"))
        n=3
        self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, outline=j.couleur,tags=("miseajour","Etoile",i.id,j.nom,"Etoilemere"))

    def initEspace(self, evt=""):  # Methode general qui creer les etoiles et positionnent l'artefact( planete mere)
        self.afficheEtoiles()
        self.afficheArtefact()


    def afficheArtefact(self): # Affiche le vaisseau de depart
        self.afficherCadreInfoEntite()
        self.canevasEspace.delete("artefact")
        self.canevasPlanete.delete("artefact")

        for i in self.partie.civs.keys():                           # boucle parcourant le dictionaire des civilisations
            coul=self.partie.civs[i].couleur                        # identifie la couleur de chaque online user
            n=5
            for j in self.partie.civs[i].entites["Station"]:
                self.canevasEspace.create_rectangle(j.x-n,j.y-n,j.x+n,j.y+n,outline=coul,fill=coul,tags=("artefact",i,j.id,"Station"))
                self.canevasEspace.create_oval(j.x-n,j.y-n,j.x+n,j.y+n,outline=coul,fill="lightblue",tags=("artefact",i,j.id,"Station"))

            for j in self.parent.modele.civs[i].entites.keys():
                if j == "Vaisseaux":
                    self.h=12
                    for z in self.parent.modele.civs[i].entites[j]:
                        n=z.grosseur
                        if z.type is "Scout":
                            #self.canevasEspace.create_arc(z.x-n,z.y-n,z.x+n,z.y+n,start=(90-22), extent=45,outline=coul,fill="white",tags=("artefact",i,z.id,"Vaisseaux"))
                            self.canevasEspace.create_image(z.x-self.imageScout.width()/2,z.y-self.imageScout.height()/2,image=self.imageScout,anchor=NW,tags=("artefact",i,z.id,"Vaisseaux"))
                        elif z.type is "Corvette":
                            self.canevasEspace.create_image(z.x-self.imageCorvette.width()/2,z.y-self.imageCorvette.height()/2,image=self.imageCorvette,anchor=NW,tags=("artefact",i,z.id,"Vaisseaux"))
                        elif z.type is "Colonisateur":
                            self.canevasEspace.create_image(z.x-self.imageColonisateur.width()/2,z.y-self.imageColonisateur.height()/2,image=self.imageColonisateur,anchor=NW,tags=("artefact",i,z.id,"Vaisseaux"))
                        elif z.type is "Fregate":
                            self.canevasEspace.create_image(z.x-self.imageFregate.width()/2,z.y-self.imageFregate.height()/2,image=self.imageFregate,anchor=NW,tags=("artefact",i,z.id,"Vaisseaux"))
                        elif z.type is "Destroyer":
                            self.canevasEspace.create_image(z.x-self.imageDestroyer.width()/2,z.y-self.imageDestroyer.height()/2,image=self.imageDestroyer,anchor=NW,tags=("artefact",i,z.id,"Vaisseaux"))

                elif j == "Batiments":
                    n=7
                    for z in self.parent.modele.civs[i].entites[j]:
                        if self.planeteSelectionnee in self.parent.modele.civs[i].planetesColonisees:
                            if z in self.planeteSelectionnee.entite[j]:
                                if self.planeteSelectionnee.noPlanete == z.idPlanete:
                                    if z.type is "CollecteurGaz":
                                        self.canevasPlanete.create_rectangle(z.x-n,z.y-n,z.x+n,z.y+n,outline=coul,fill=coul,tags=("artefact",i,z.id,"Batiments"))
                                        self.canevasPlanete.create_oval(z.x-n,z.y-n,z.x+n,z.y+n,outline=coul,fill="green",tags=("artefact",i,z.id,"Batiments"))
                                    elif z.type is "CollecteurMinerais":
                                        self.canevasPlanete.create_rectangle(z.x-n,z.y-n,z.x+n,z.y+n,outline=coul,fill=coul,tags=("artefact",i,z.id,"Batiments"))
                                        self.canevasPlanete.create_oval(z.x-n,z.y-n,z.x+n,z.y+n,outline=coul,fill="green",tags=("artefact",i,z.id,"Batiments"))
                                        self.canevasPlanete.create_rectangle(z.x-n,z.y-n,z.x+n,z.y+n,outline=coul,fill=coul,tags=("artefact",i,z.id,"Batiments"))
                                        self.canevasPlanete.create_oval(z.x-n,z.y-n,z.x+n,z.y+n,outline=coul,fill="red",tags=("artefact",i,z.id,"Batiments"))
                                    elif z.type is "Cosmolab":
                                        self.canevasPlanete.create_rectangle(z.x-n,z.y-n,z.x+n,z.y+n,outline=coul,fill=coul,tags=("artefact",i,z.id,"Batiments"))
                                        self.canevasPlanete.create_oval(z.x-n,z.y-n,z.x+n,z.y+n,outline=coul,fill="blue",tags=("artefact",i,z.id,"Batiments"))
                                    elif z.type is "CommandCenter":
                                        print("CommandCenter a dessiner")
                                        self.canevasPlanete.create_oval(z.x-n,z.y-n,z.x+n+35,z.y+n+35,outline=coul,fill="black",tags=("artefact",i,z.id,"Batiments"))
                                        self.canevasPlanete.create_oval(z.x-n+10,z.y-n+10,z.x+n+25,z.y+n+25,outline=coul,fill="red",tags=("artefact",i,z.id,"Batiments"))
                                        self.canevasPlanete.create_oval(z.x-n+13,z.y-n+13,z.x+n+22,z.y+n+22,outline=coul,fill="yellow",tags=("artefact",i,z.id,"Batiments"))
                                        #self.canevasPlanete.create_image(z.x-self.imageCommandCenter.width()/2,z.y-self.imageCommandCenter.height()/2,image=self.imageCommandCenter,anchor=NW,tags=("artefact",i,z.id,"Batiments"))

        self.afficheSelection()

        if self.partie.civs[self.parent.nom].listeChat and self.parent.modele.chatSent:
            self.afficheChat(self.partie.civs[self.parent.nom].listeChat)

    def afficheSelection(self): # Affiche la selection
        self.canevasEspace.delete("selection")
        for i in self.selection:
            if i.type=="Station":
                n=9 # grandeur du carre
                d=1   #decallage vers le haut
                d=1   #decallage vers le haut
            elif i.type=="Vaisseaux":
                n=i.grosseur/2+3  # grandeur du carre
                d=5   #decallage vers le haut
            else:
                n=self.h/2+3  # grandeur du carre
                d=5   #decallage vers le haut
            x=i.x
            y=i.y
            self.canevasEspace.create_rectangle(x-n,y-n-d,x+n,y+n-d,outline="yellow",dash=( 2, 2 ),  tags=("selection",))
            #print ("Affiche Selection")

    def changeCible(self,evt):
        #s=input("WO")
        #print("CHANGE",e.x,e.y)
        x=evt.x
        y=evt.y
        x1 = self.canevasEspace.canvasx(x)
        y1 = self.canevasEspace.canvasy(y)
        if len(self.selection)==1:
            count=0
        else:
            count=0-int(len(self.selection)/2)*15
        for i in self.selection: #Liste de id
            print ("Change Cible")
            self.parent.changeCible(i.id,x1,y1+count)
            count=count+15
        print ("Change Cible")

    def creerServeur(self):
        nom=self.captureJoueur.get()
        leip=self.parent.monip
        if nom:
            pid=self.parent.creerServeur()
            if pid:
                self.demarreB.config(state=NORMAL)
                self.root.after(500,self.inscritClient)
        else:
            mb.showerror(title="Besoin d'un nom",message="Vous devez inscrire un nom pour vous connecter.")

    def inscritClient(self):
        nom=self.captureJoueur.get()
        leip=self.parent.monip
        self.parent.inscritClient(nom,leip)

    def connecterServeur(self):
        nom=self.captureJoueur.get()
        leip=self.autreip.get()
        if nom:
            self.parent.inscritClient(nom,leip)

    def afficheEtoiles(self): #dessine les etoiles dans l'espace seulement
        self.canevasEspace.delete("Etoile")
        taille=20
        for i in self.partie.etoiles:
            #self.canevasEspace.create_oval(i.x-taille,i.y-taille,i.x+taille,i.y+taille,fill="yellow",tags=("Etoiles",i.id,"  ",len(i.planetes)))
            self.canevasEspace.create_image(i.x-self.imageEtoile.width()/2,i.y-self.imageEtoile.height()/2,image=self.imageEtoile,anchor=NW,tags=("Etoiles",i.id,"  ",len(i.planetes)))
        etoileMere=self.partie.civs[self.parent.nom].planeteMere.parent
        taille=40
        self.canevasEspace.create_oval( etoileMere.x-taille, etoileMere.y-taille, etoileMere.x+taille, etoileMere.y+taille,width=2,outline="red",tags=("Etoiles",self.parent.nom, etoileMere.id,"Etoilemere"))

    def centrerPlanete(self): # lorsqu'on clique sur " trouver planete " : methode centre planete mere
        self.centrerObjet( self.partie.civs[self.parent.nom].planeteMere.parent)

    def centrerObjet( self, obj):
        x=obj.x
        y=obj.y

        sx = float(self.x_espace)
        ecranx=float( self.canevasEspace.winfo_width() )/2.0
        posx = ( x-ecranx )/sx
        self.canevasEspace.xview( "moveto", posx )

        sy = float( self.y_espace )
        ecrany=float( self.canevasEspace.winfo_height() )/2.0
        posy = ( y-ecrany )/sy
        self.canevasEspace.yview( "moveto", posy )

    def centrerVuePlanete(self):
        x=self.x_planete/2-((self.x_planete/1.2)/2)
        y=self.y_planete/2-((self.y_planete/1.4)/2)

        sx=float(self.x_planete)
        ecranx=float(self.canevasPlanete.winfo_width() )/2.0
        posx=(x-ecranx)/sx
        self.canevasPlanete.xview("moveto",posx)

        sy=float(self.y_planete)
        ecrany=float(self.canevasPlanete.winfo_height() )/2.0
        posy=(y-ecrany)/sy
        self.canevasPlanete.yview("moveto",posy)

    def creeCadreInferieur(self):
        self.cadreInferieur=Frame(self.cadrePartie, height=175, bg="black")
        self.creeCadreChat()
        self.creeCadreInfoEntite()
        self.creeCadreBoutons()
        
    def afficherBoutonsSelection(self):
        if self.boutonS == 2:
            self.deleteBoutons()
            self.bVaisseau.grid(row=2, column=0, sticky=W+E, pady=3, columnspan=1)
            self.bVaisseau1.grid(row=2, column=1, sticky=W+E, pady=3, columnspan=1)
        elif self.boutonS == 1:
            self.deleteBoutons()
            print("Vide")
        elif self.boutonS == 3:
            self.deleteBoutons()
            self.creeScout.grid(row=2, column=0, sticky=W+E, pady=3, columnspan=1)
            self.creeCorvette.grid(row=2, column=1, sticky=W+E, pady=3, columnspan=1)
            self.creeColonisateur.grid(row=2, column=2, sticky=W+E, pady=3, columnspan=1)
            self.creeFregate.grid(row=3, column=1, sticky=W+E, pady=3, columnspan=1)
            self.creeDestroyer.grid(row=3, column=2, sticky=W+E, pady=3, columnspan=1)
            print("Station")
        elif self.boutonS == 4:
            self.deleteBoutons()
            self.creeStation.grid(row=2, column=0, sticky=W+E, pady=3, columnspan=1)
            self.creeCollecteurGaz.grid(row=2, column=1, sticky=W+E, pady=3, columnspan=1)
            self.creeCollecteurMinerais.grid(row=4, column=1, sticky=W+E, pady=3, columnspan=1)
            self.creeSpatiolab.grid(row=2, column=3, sticky=W+E, pady=3, columnspan=1)
            print("Etoile")
            
        elif self.boutonS == 5:
            self.deleteBoutons()
            self.creeCommandCenter.grid(row=2, column=2, sticky=W+E, pady=3, columnspan=1)
            print("Planete")
            
        elif self.boutonS == 6:
            self.deleteBoutons()
            self.bVaisseau.grid(row=2, column=0, sticky=W+E, pady=3, columnspan=1)
            self.coloniser.grid(row=2, column=3, sticky=W+E, pady=3, columnspan=1)
            
            
        elif self.boutonS == 7:
            self.deleteBoutons()
            self.creeSpatiolab.grid(row=2, column=3, sticky=W+E, pady=3, columnspan=1)
            self.creeCollecteurGaz.grid(row=2, column=1, sticky=W+E, pady=3, columnspan=1)
            self.creeCollecteurMinerais.grid(row=3, column=1, sticky=W+E, pady=3, columnspan=1)
            print("commandCenter")

        elif self.boutonS == 8:
            self.deleteBoutons()
            self.upgradeVitesse.grid(row=2, column=3, sticky=W+E, pady=3, columnspan=1)
            self.bVaisseau1.grid(row=2, column=4, sticky=W+E, pady=3, columnspan=1)
            print("cosmoslab")

        elif self.boutonS == 9:
            self.deleteBoutons()
            self.upgradeGaz.grid(row=2, column=4, sticky=W+E, pady=3, columnspan=1)
            print("collecteurgaz")
            
        elif self.boutonS == 10:
            self.deleteBoutons()
            self.upgradeMinerais.grid(row=2, column=3, sticky=W+E, pady=3, columnspan=1)
            print("collecteurMinerai")

    def creeCadreChat(self):
        self.cadreChat=Frame(self.cadreInferieur, width=400, height=100, bg="white")
        self.cadreListe=Frame(self.cadreInferieur)
        scrollbar = Scrollbar(self.cadreListe, orient=VERTICAL)
        self.displayChat=Listbox(self.cadreListe, width=45, height=5, bg="gray", yscrollcommand=scrollbar.set)
        scrollbar.config(command=self.displayChat.yview)
        scrollbar.grid(row=1,column=1,sticky=N+S)
        self.cadreChat.grid(row=0,column=0,sticky=W, padx=10, pady=10)
        self.cadreListe.grid(row=1, column=0)
        self.displayChat.grid(row=1,column=0,sticky=N+S+E+W, padx=10, pady=10)
        self.champChat=Entry(self.cadreChat, width=50)
        self.champChat.bind("<Return>",self.captureChat)

    def creeCadreBoutons(self):
        self.cadreBoutons=Frame(self.cadreInferieur, width=450, height=108, bg="green", highlightthickness=2)
        self.cadreBoutons.grid(row=0,column=2, sticky=W+S+E+N, padx=10, pady=10, rowspan=2)
        self.cadreBoutons.grid_propagate(0)
        self.bVaisseau = Buttonjm(self.cadreBoutons,text="Detruire", command=self.supprimer)
        #self.bVaisseau.grid(row=5, column=2, sticky=W+E, pady=3, columnspan=1)
        self.bVaisseau1 = Buttonjm(self.cadreBoutons,text="Upgrade l'attaque", command= self.upgradePower)
        #self.bVaisseau1.grid(row=2, column=0, sticky=W+E, pady=3, columnspan=1)
        self.creeScout=Buttonjm(self.cadreBoutons,text="Creer Scout",command=self.ajouterScout)
        #self.creeScout.grid(row=3, column=0, sticky=W+E, pady=3, columnspan=1)
        self.creeCorvette=Buttonjm(self.cadreBoutons,text="Creer Corvette ",command=self.ajouterCorvette)
        #self.creeCorvette.grid(row=3, column=1, sticky=W+E, pady=3, columnspan=1)
        self.creeColonisateur=Buttonjm(self.cadreBoutons,text="Creer Colonisateur",command=self.ajouterColonisateur)
        self.creeFregate=Buttonjm(self.cadreBoutons,text="Creer Fregate",command=self.ajouterFregate)
        self.creeDestroyer=Buttonjm(self.cadreBoutons,text="Creer Destroyer",command=self.ajouterDestroyer)
        #self.creeColonisateur.grid(row=3, column=2, sticky=W+E, pady=3, columnspan=1)
        self.creeCollecteurGaz=Buttonjm(self.cadreBoutons,text="Creer Collecteur a Gaz",command=self.ajouterCollecteurGaz)
        #self.creeCollecteurGaz.grid(row=4, column=0, sticky=W+E, pady=3, columnspan=1)
        self.creeCollecteurMinerais=Buttonjm(self.cadreBoutons,text="Creer Collecteur a Minerais",command=self.ajouterCollecteurMinerais)
        self.creeCollecteurMinerais.config(state=DISABLED)
        #self.creeCollecteurMinerai.grid(row=4, column=1, sticky=W+E, pady=3, columnspan=1)
        self.creeCommandCenter=Buttonjm(self.cadreBoutons,text="Creer Command Center",command=self.ajouterCommandCenter)
        #self.creeCommandCenter.grid(row=4, column=2, sticky=W+E, pady=3, columnspan=1)
        self.creeSpatiolab=Buttonjm(self.cadreBoutons,text="Creer Spatiolab",command=self.ajouterCosmolab)
        self.creeSpatiolab.config(state=DISABLED)
        #self.creeSpatiolab.grid(row=4, column=3, sticky=W+E, pady=3, columnspan=1)
        self.creeStation=Buttonjm(self.cadreBoutons,text="Creer Station",command=self.ajouterStation)
        #self.creeStation.grid(row=4, column=4, sticky=W+E, pady=3, columnspan=1)
        self.coloniser=Buttonjm(self.cadreBoutons,text="Coloniser !",command=self.colonisation)
        self.upgradeVitesse=Buttonjm(self.cadreBoutons,text="Upgrade la vitesse",command=self.upgradeVitesse)
        self.upgradeMinerais=Buttonjm(self.cadreBoutons,text="Upgrade Minerais", command=self.upgradeMinerais)
        self.upgradeGaz=Buttonjm(self.cadreBoutons,text="Upgrade gaz", command=self.upGaz)
        
    def creeCadreInfoEntite(self):
        self.cadreInfoEntite=Frame(self.cadreInferieur, width=400, height=120, bg="grey25", highlightthickness=2)
        self.cadreInfoEntite.grid(row=0, column=1, padx=10, pady=10, rowspan=2,sticky=N+S+E+W)
        self.cadreInfoEntite.grid_propagate(0)
        self.cadreInfoEntite.columnconfigure(0,weight=1)
        self.cadreInfoEntite.columnconfigure(1,weight=1)
        self.cadreInfoEntite.rowconfigure(0,weight=1)
        self.cadreInfoEntite.rowconfigure(1,weight=1)
        self.cadreInfoEntite.rowconfigure(2,weight=1)
        self.cadreInfoEntite.rowconfigure(3,weight=1)
        self.cadreInfoEntite.rowconfigure(4,weight=1)
        self.tagType = Label(self.cadreInfoEntite,textvariable=self.textType, font=Font(size=18), fg="white", bg="grey25")
        self.tagType.grid(column=1,row=1)
        self.tagVie = Label(self.cadreInfoEntite,textvariable=self.textVie, font=Font(size=11), fg="white", bg="grey25")
        self.tagVie.grid(column=0,row=4)
        self.tagArmee = Label(self.cadreInfoEntite,textvariable=self.textArmee, font=Font(size=10), fg="white", bg="grey25")
        self.tagArmee.grid(column=1,row=4, sticky=W)
        self.tagGaz = Label(self.cadreInfoEntite,textvariable=self.textGaz, font=Font(size=10), fg="white", bg="grey25")
        self.tagGaz.grid(column=1,row=4, sticky=W)
        self.tagMinerai = Label(self.cadreInfoEntite,textvariable=self.textMinerai, font=Font(size=10), fg="white", bg="grey25")
        self.tagMinerai.grid(column=1,row=5, sticky=W)
        
        #self.test = Label(self.cadreInfoEntite,text="TOTO", font=Font(size=12), fg="orange", bg="grey25")
        #self.test.grid(column=0,row=0)
        self.cadreVie = Frame(self.cadreInfoEntite, width=50, height=50, bg="green", highlightthickness=1, highlightcolor="white", pady=10)
        self.cadreVie.grid(column=0,row=1)
        self.cadreVie.grid_propagate(0)
        self.cadreVie.grid_remove()
        
    def afficherCadreInfoEntite(self):
        if self.perspectiveCouranteTexte=="Planete":
            if self.selection:
                self.cadreVie.grid_remove()
                self.entite = self.selection[0]
                self.textType.set(self.entite.type)
                self.textVie.set(str(self.entite.vie)+"/"+str(self.entite.initVie))
                self.textArmee.set("            ")
                self.textGaz.set("Gaz: "+str(self.entite.gazPlanete))
                self.textMinerai.set("Minerai: "+str(self.entite.minPlanete))
                self.cadreVie.grid()
            else:
                self.textType.set("           ")
                self.textVie.set("           ")
                self.textArmee.set("           ")
                self.textGaz.set("           ")
                self.textMinerai.set("           ")
                self.cadreVie.grid_remove()
        elif self.perspectiveCouranteTexte=="Systeme": 
            if self.selection :
                self.cadreVie.grid_remove()
                self.entite = self.selection[0]
                self.textType.set(self.entite.type)
                self.textVie.set(str(self.entite.vie)+"/"+str(self.entite.initVie))
                self.textArmee.set("            ")
                self.textGaz.set("Gaz: "+str(self.entite.gazPlanete))
                self.textMinerai.set("Minerai: "+str(self.entite.minPlanete))
                self.cadreVie.grid()
            else:
                self.textType.set("           ")
                self.textVie.set("           ")
                self.textArmee.set("           ")
                self.textGaz.set("           ")
                self.textMinerai.set("           ")
                self.cadreVie.grid_remove()
        else:
            if self.selection:
                self.cadreVie.grid_remove()
                self.entite = self.selection[0]
                self.textType.set(self.entite.type)
                self.textVie.set(str(self.entite.vie)+"/"+str(self.entite.initVie))
                self.textArmee.set("Armee: "+str(len(self.selection)))
                self.cadreVie.grid()
            else:
                self.textType.set("           ")
                self.textVie.set("           ")
                self.textArmee.set("           ")
                self.textGaz.set("           ")
                self.textMinerai.set("           ")
                self.cadreVie.grid_remove()

    def dessineEtoileSelectionnee(self):
        x=self.etoileSelectionnee.x
        y=self.etoileSelectionnee.y
        self.canevasEspace.create_oval(x-30,y-30,x+30,y+30, outline="blue",tags=("EtoileSelectionnee"))
    
    def griserBoutonPerspective(self,pd,pa):
        #pd=perspectivedepart
        #pa=perspectivearrivee
        print("griser")
        if pa=="Espace":
            self.bespace.config(state=DISABLED)
        elif pa=="Systeme":
            self.bsysteme.config(state=DISABLED)
        elif pa=="Cosmos":
            self.bcosmos.config(state=DISABLED)
        else:
            self.bplanete.config(state=DISABLED)
        if pd=="Espace":
            self.bespace.config(state=NORMAL)
        elif pd=="Systeme":
            self.bsysteme.config(state=NORMAL)
        elif pd=="Cosmos":
            self.bcosmos.config(state=NORMAL)
        else:
            self.bplanete.config(state=NORMAL)

    def placeCosmos(self):
        self.placeCadreNiveau("Cosmos")
    def placeSysteme(self):
        self.placeCadreNiveau("Systeme")
    def placeEspace(self):
        self.placeCadreNiveau("Espace")
    def placePlanete(self):
        self.placeCadreNiveau("Planete")
        
    def drawLaser(self,obj1,obj2):
        x1=obj1.x
        x2=obj2.x
        y1=obj1.y
        y2=obj2.y
        tag=str(obj1.id)+"_laser"
        self.canevasEspace.create_line(x1,y1,x2,y2, fill="yellow",tags=(tag))
        self.canevasEspace.create_line(x1-1,y1+1,x2+1,y2-1, fill="blue",tags=(tag))
        self.canevasEspace.create_line(x1+1,y1-1,x2-1,y2+1, fill="red",tags=(tag))
        #print("drawLaser")
        #print(tag)
        
    def deleteLaser(self,obj):
        tag=str(obj.id)+"_laser"
        self.canevasEspace.delete(tag)

    def supprimer(self):
        print("delete", self.objetSelect, self.objetSelect.id)
        tag=self.objetSelect
        for i in self.selection:
          self.parent.supprimer(tag)
                             
    def upgradeVitesse(self):
        tag = self.objetSelect
        for i in self.selection:
            self.parent.upgradeVitesse(tag)
            
    def upgradePower(self):
        tag = self.objetSelect
        for i in self.selection:
            self.parent.upgradePower(tag)
            
    def upgradeMinerais(self):
        tag = self.objetSelect
        for i in self.selection:
            self.parent.upgradeMinerais(tag)
            
    def upGaz(self):
        tag = self.objetSelect
        for i in self.selection:
            self.parent.upGaz(tag)
        
    def deleteBoutons(self):
        self.bVaisseau.grid_remove()
        self.bVaisseau1.grid_remove()
        self.creeScout.grid_remove()
        self.creeCorvette.grid_remove()
        self.creeColonisateur.grid_remove()
        self.creeFregate.grid_remove()
        self.creeDestroyer.grid_remove()
        self.creeCollecteurGaz.grid_remove()
        self.creeCollecteurMinerais.grid_remove()
        self.creeCommandCenter.grid_remove()
        self.creeSpatiolab.grid_remove()
        self.creeStation.grid_remove()
        self.coloniser.grid_remove()
        self.upgradeVitesse.grid_remove()
        self.upgradeGaz.grid_remove()
        self.upgradeMinerais.grid_remove()
        
    def verifieEcranBouge(self):
        
        #Si c'est a gauche
        if(self.posSourisXDansEcran<=self.posFenetreXDansEcran+20):
            if self.perspectiveCouranteTexte=="Espace":
                self.canevasEspace.xview('scroll',-1,'units')
            elif self.perspectiveCouranteTexte=="Planete":
                self.canevasPlanete.xview('scroll',-1,'units')
        #Si c'est a droite
        elif self.posFenetreXDansEcran+self.root.winfo_width()-20<=self.posSourisXDansEcran<=self.posFenetreXDansEcran+self.root.winfo_width():
                if self.perspectiveCouranteTexte=="Espace":
                    self.canevasEspace.xview('scroll',1,'units')
                elif self.perspectiveCouranteTexte=="Planete":
                    self.canevasPlanete.xview('scroll',1,'units')
        #Si c'est en bas
        if self.posFenetreYDansEcran+self.root.winfo_height()-20<=self.posSourisYDansEcran<=self.posFenetreYDansEcran+self.root.winfo_height():
                if self.perspectiveCouranteTexte=="Espace":
                    self.canevasEspace.yview('scroll',1,'units')
                elif self.perspectiveCouranteTexte=="Planete":
                    self.canevasPlanete.yview('scroll',1,'units')
        elif self.posSourisYDansEcran<=self.posFenetreYDansEcran+20:
            if self.perspectiveCouranteTexte=="Espace":
                self.canevasEspace.yview('scroll',-1,'units')
            elif self.perspectiveCouranteTexte=="Planete":
                self.canevasPlanete.yview('scroll',-1,'units')
        
        
    #Donne les coordonnes du pointeur de la souris lorsqu'elle est bougee
    def xy_motionWindow(self,evt=""):
        self.posSourisXDansEcran=evt.x_root
        self.posSourisYDansEcran=evt.y_root
        self.posFenetreXDansEcran=self.root.winfo_rootx()
        self.posFenetreYDansEcran=self.root.winfo_rooty()
        
    def drawSelectionBox(self,evt):
        print("SelectionBox")
        print(self.selectX,self.selectY)
        print(evt.x, evt.y)
        x = self.canevasEspace.canvasx(evt.x)
        y = self.canevasEspace.canvasy(evt.y)
        self.canevasEspace.delete("selectionBox")
        self.canevasEspace.create_rectangle(self.selectX,self.selectY,x,y, outline="green",tags=("selectionBox",))
        self.canevasEspace.update_idletasks()
        self.selectionBox=True
        
    def dessinePlaneteSelectionne(self):
        x=self.planeteSelectionnee.x
        y=self.planeteSelectionnee.y
        t=self.planeteSelectionnee.taille
        t=t+20
        divx=self.x_systeme/self.canevasSysteme.winfo_width()
        divy=self.y_systeme/self.canevasSysteme.winfo_height()
        x=x/divx
        y=y/divy
        self.canevasSysteme.create_oval(x-t,y-t,x+t,y+t, outline="blue",tags=("planeteSelectionnee"))
        
    def releaseSelectionBox(self,evt):
        if self.selectionBox:
            self.afficherCadreInfoEntite()
            print("Button-release")
            self.canevasEspace.delete("selectionBox")
            x = self.canevasEspace.canvasx(evt.x)
            y = self.canevasEspace.canvasy(evt.y)
            tags=self.canevasCourant.find_enclosed(self.selectX,self.selectY,x,y)
            self.selection=[]
            
            self.selectionBox=False
            print(self.selection)
            for i in tags:
                self.t=self.canevasCourant.gettags(i)
                self.addObject()

    def colonisation(self):
        valid = True
        print(self.objetSelect)
        for i in self.modele.etoiles:
            for j in self.modele.civs[self.parent.nom].etoilesVisites:
                if i.id == j.id:
                    valid = False
                    print("Position Etoile :",j.x,j.y)
                    break
            print(valid)
            if valid:
                x=abs(i.x-self.objetSelect.x)
                y=abs(i.y-self.objetSelect.y)
                print(x,y)
                if x < 20 and y < 20:
                    if self.modele.civs[self.parent.nom].gaz >= self.modele.civs[self.parent.nom].prix["gazStationOrbital"]:
                        if self.modele.civs[self.parent.nom].minerai >= self.modele.civs[self.parent.nom].prix["minStationOrbital"]:
                            self.supprimer()
                            self.objetSelect=i
                            self.modele.civs[self.parent.nom].etoilesVisites.append(i)
                            for j in i.planetes:
                                self.modele.civs[self.parent.nom].planetesColonisees.append(j)
                            print("Nouvelle Etoile ?",self.modele.civs[self.parent.nom].etoilesVisites)
                            print("Nouvelle Planetes ?",self.modele.civs[self.parent.nom].planetesColonisees)
                            self.ajouterStation()
                            break
                        else:
                            print("Pas assez d'argent")
                    else:
                        print("Pas assez d'argent")
                else:
                    print("Pas assez proche d'une �toile")

    def initMort(self):
        mb.showinfo("Vous etes mort","Noob")
        
